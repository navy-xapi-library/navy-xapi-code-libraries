import { ACTIVITY } from './constants/constants';
import { IConfiguration, Interaction, IAssessmentParent, Result, ICourseAttempt, IAttachment, InteractionComponentList, IInteractionComponentListItem, IStatementConfiguration } from './interfaces';
export declare class ELearningXAPI {
    private _ADL;
    private _config;
    private _lessonRegistration;
    private _courseRegistration;
    private _statementObject;
    private _statement;
    private _extensions;
    private _activityID;
    private _platform;
    private _activityState;
    private _currentLessonAttempt;
    /**
     * @constructor
     * @param {Configuration} config - A configuration object that contains auth credentials to an lrs endpoint
     */
    constructor(config?: IConfiguration);
    /**
     * Store initialize value to true for a Course or lesson attempt activity state and send an initialized statement to the configured LRS.
     * @param {string} activityType
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    initialize: (activityType: ACTIVITY) => Promise<Result>;
    /**
     * Suspend an attempt using activity state and send a suspend statement to the configured LRS.
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    suspend: (activityId?: string | undefined) => Promise<Result>;
    /**
     * Resume an attempt using stored activity state or passed in activityId and send a resume statement to the configured LRS.
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    resume: (activityId?: string | undefined) => Promise<Result>;
    /**
     * Set terminated value to true for a Course or Lesson attempt activity state and send an terminated statement to the configured LRS.
     * @param {string} activityType
     * @param {string} duration
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    terminate: (activityType: ACTIVITY, duration?: string | undefined) => Promise<Result>;
    /**
     * Sends completion statement to the configured LRS.
     * @param {boolean} completed - A boolean value representing if a user has completed an activity
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    setComplete: (completed: boolean) => Promise<Result> | Result;
    /**
     * Sends success statement to the configured LRS.
     * @param {boolean} completed - A boolean value representing if a user pass or fail a success criteria.
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    setSuccess: (success: boolean) => Promise<Result> | Result;
    /**
     * Sends success statement to the configured LRS.
     * @param {string} type - The score type. For instance, scaled, raw, min, or max.
     * @param {string} score - The learner's score value.
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    setScore: (type: string, score: number) => Promise<Result> | Result;
    /**
     * Set the bookmark property to the location value within the configured LRS activity state
     * @param {string} location - A string describing a location.
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    setBookmark: (location: string) => Promise<Result>;
    /**
     * Returns the bookmark property value within the activity state
     * @returns {string}
     */
    getBookmark: () => string | undefined;
    /**
     * Creates an interaction definition object to be used within the statement "object" property body
     * @param {string} interactionType - A predefined String value for the type of interaction.
     * @param {string} id - A unique identifier for the interaction according to the Activity ID requirements in the NETC Common Reference Profile
     * @param {string} name - A value that represents the official name or title of the interaction activity.
     * @param {string} description - A value that represents a short description of the interaction activity.
     * @param {Array} correctResponsesPattern - An array of patterns representing the correct response to the interaction.
     * @param {Array} interactionComponentList - An array list of interaction component(s) list or an object with a source and target property which values are arrays of interaction component list for a matching interaction type.
     * @returns {Object <Interaction>}
     */
    createInteraction: (interactionType: string, id: string, name: string, description: string, correctResponsesPattern: [], interactionComponentList: InteractionComponentList) => Interaction;
    /**
     * Sends interaction response to the configured LRS.
     * @param {Array <strings} response - An array that holds the response(s) of an interaction.
     * @param {Object <Interaction>} interaction - The interaction object defining the interaction activity
     * @param {Object <AssessmentParent>} parent - The parent configuration object.
     * @param {string} otherResponse - A provided extra response if an interaction has multiple responses
     * @param {Array <IAttachment>} attachments - An array of attachment objects
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    sendInteraction: (response: string[], interaction: Interaction, parent: IAssessmentParent, otherResponse: string | undefined, attachments: IAttachment[]) => Promise<Result> | Result;
    /**
     * Send a "viewed" statement to the configured LRS.
     * @param {string} id
     * @param {string} name
     * @param {string} description
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    viewed: (id: string, name: string, description: string) => Promise<Result>;
    /**
     * Send a "section" statement to the configured LRS.
     * @param {string} id
     * @param {string} name
     * @param {string} description
     * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
     */
    section: (id: string, name: string, description: string) => Promise<Result>;
    /**
   * Send a "exited" statement to the configured LRS.
   * @param {string} id
   * @param {string} name
   * @param {string} description
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
    exit: (id: string, name: string, description: string) => Promise<Result>;
    /**
     * Sets activity definition to the statement object
     * @param {string} id
     * @param {string} name
     * @param {string} description
     */
    setActivity(id: string, name?: string, description?: string): void;
    /**
     * Sets parent definition to the statement object
     * @param {string} id - A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile
     * @param {string} name - A value that represents  the official name or title of the Activity
     * @param {string} description - A value that represents a short description of the Activity
     */
    setParent(id: string, name: string, description: string, type: string): void;
    /**
     * Sets group definition to the statement object
     * @param {string} id - A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile
     * @param {string} name - A value that represents  the official name or title of the Activity
     * @param {string} description - A value that represents a short description of the Activity
     */
    setGroup(id: string, name: string, description: string): void;
    /**
     * Adds a single extension or an object of extensions to the statement object
     * @param {string} key - string identifer of the extension
     * @param {string} value - value of the extension
     */
    addExtensions(key: string | {}, value?: string): void;
    createParent: (id: string, name?: string | undefined, description?: string | undefined) => IAssessmentParent;
    createInteractionComponentList: (id: string, description: string) => IInteractionComponentListItem;
    createResponsePattern: (interactionType: string, response: string[]) => string[];
    private formatResponsePattern;
    attachment: (value: any, usageType: string, contentType: string, display: string, description: string) => IAttachment;
    private getUsageType;
    getLessonRegistration: () => string;
    getCourseRegistration: () => string;
    launch: () => Promise<Result>;
    private isTinCanLaunch;
    private adlLaunch;
    private createCourseAttemptState;
    private createLessonAttemptState;
    private addLessonAttempt;
    private updateCourseActivityState;
    private updateLessonActivityState;
    /**
     *
     * @param activityID
     * @param registration
     * @returns
     */
    getActivityState: (activityID: string) => Promise<ICourseAttempt>;
    /**
     *
     * @param id
     * @param registration
     * @param attempt
     * @returns
     */
    private setActivityState;
    /**
     * Sends statement
     * @param configuration
     * @param [attachments]
     * @returns statement
     */
    private sendStatement;
    private set lessonRegistration(value);
    private get lessonRegistration();
    private set courseRegistration(value);
    private get courseRegistration();
    get ACTIVITY(): typeof ACTIVITY;
    get VERB(): {
        INITIALIZED: string;
        COMPLETED: string;
        PASSED: string;
        FAILED: string;
        TERMINATED: string;
        SUSPENDED: string;
        RESUMED: string;
        RESPONDED: string;
        SCORED: string;
        BOOKMARKED: string;
        VIEWED: string;
        ACCESSED: string;
        EXITED: string;
    };
    get INTERACTION(): {
        TRUE_FALSE: string;
        CHOICE: string;
        CHOICE_WITH_EXPLANATION: string;
        FILL_IN: string;
        LONG_FILL_IN: string;
        MATCHING: string;
        PERFORMANCE: string;
        SEQUENCING: string;
        LIKERT: string;
        NUMERIC: string;
        OTHER: string;
        OTHER_UPLOAD: string;
        OTHER_UPLOAD_ATTACHMENT: string;
    };
    get SCORE(): {
        SCALED: string;
        RAW: string;
        MIN: string;
        MAX: string;
    };
    get EXTENSIONS(): {
        SCHOOL_CENTER: string;
        LAUNCH_LOCATION: string;
        USER_AGENT: string;
        RESPONSE_EXPLANATION: string;
        RESPONSE_TYPE: string;
        EXTENDED_INTERACTION_TYPE: string;
    };
    get ACTIVITY_TYPES(): {
        LESSON: string;
        COURSE: string;
        ASSESSMENT: string;
        SECTION: string;
        PROFILE: string;
        PAGE: string;
    };
    get statement(): string;
    get activity(): Partial<IStatementConfiguration>;
}
