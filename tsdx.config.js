module.exports = {
    rollup(config) {
      config.output.esModule = true
      if (config.output.format === 'umd') { 
        delete config.external;
      }
      return config;
    }
  }