const fs = require('fs');

const packageJson = {
    "name": "navy",
    "version": "1.0.0",
    "description": "A set of Javascript Libraries to help simplify communication to an LRS and to help track interactions and learning activities at a granular level as well as provide the tools to help aid in one's Performance.",
    "main": "navy.umd.production.min.js",
    "types": "./index.d.ts",
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    "author": "Veracity Technology Consultants",
    "license": "ISC"
}

fs.writeFile('./dist/package.json', JSON.stringify(packageJson, undefined, 2), err => {
    if(err) {
        console.log(err);
        return;
    }
})