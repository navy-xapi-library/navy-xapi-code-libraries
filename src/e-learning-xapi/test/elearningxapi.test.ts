// const ELearningXAPI = require('../src/index')
import { ELearningXAPI } from '../../../dist/veracity.umd.development.js';
import createMockInstance from 'jest-create-mock-instance';

const mockInitialize = jest.fn();
jest.mock('../../../dist/veracity.umd.development.js', () => {
  ELearningXAPI: jest.fn().mockImplementation(()=> ({
    intialize: mockInitialize
  }))
});

it('asdf', () => {
  const xapi = new ELearningXAPI(); 
  expect(() => xapi.initialize()).toThrow('Invalid activity type. Activity type must be either "course" or "lesson".')
})

// let xapi: ELearningXAPI;

// beforeEach(() => {
//   ELearningXAPI.mockClear();
// });


// test('should throw an error if no activity type is provided', () => {
//   const xapi = new ELearningXAPI();  
//   console.log(xapi);
//   expect(async () => {
//     xapi.initialize();
//   }).toThrow('Invalid activity type. Activity type must be either "course" or "lesson".');
// })


// it('We can check if the consumer called the class constructor', () => {
//   const xapi = new ELearningXAPI();
//   expect(ELearningXAPI).toHaveBeenCalledTimes(1);
// });

// it('should handle no activity type provided error', async () => {
//   try {
//     await xapi.initialize();
//   } catch (e) {
//     expect(e.message).toEqual(
//       'Invalid activity type. Activity type must be either "course" or "lesson".'
//     );
//   }
// });

// describe('Initialize', () => {
//   afterEach(() => {
//     jest.resetAllMocks();
//   });

//   it('should handle no activity type provided error', async () => {
//     const xapi = new ELearningXAPI();
//     let xapiSpy: jest.SpyInstance;
//     xapiSpy = jest.spyOn(xapi, 'initialize')
//   //   const spy = jest.spyOn(xapi.prototype, 'initialize');
//   //   xapi.intialize()
//   // //  xapi.initialize.mockImplementation(() => '');

//     // initializeWithoutConfig.mockResolvedValueOnce('');

//     // try {
//     //   await xapi.initialize();
//     // } catch(e) {
//     //   expect(e.message).toEqual('Invalid activity type. Activity type must be either "course" or "lesson".')
//     // }

//   });
// });
