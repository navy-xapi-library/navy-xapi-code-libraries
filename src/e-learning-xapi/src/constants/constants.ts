/** ***********************************************************************
*
* Veracity Technology Consultants CONFIDENTIAL
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Veracity Technology Consultants and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Veracity Technology Consultants
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Veracity Technology Consultants.
*/

// VERB CONSTANTS
const INITIALIZED = 'initialized';
const COMPLETED = 'completed';
const PASSED = 'passed';
const FAILED = 'failed';
const TERMINATED = 'terminated';
const SUSPENDED = 'suspended';
const RESUMED = 'resumed';
const RESPONDED = 'responded'
const SCORED = 'scored';
const BOOKMARKED = 'bookmarked';
const VIEWED = "viewed";
const ACCESSED = "accessed";
const EXITED = 'exited'

// INTERACTION TYPE CONSTANTS
const TRUE_FALSE = 'true-false';
const CHOICE = 'choice';
const CHOICE_WITH_EXPLANATION = 'choice-with-explanation';
const FILL_IN = 'fill-in';
const LONG_FILL_IN = 'long-fill-in';
const MATCHING = 'matching';
const PERFORMANCE = 'performance';
const SEQUENCING = 'sequencing';
const LIKERT = 'likert';
const NUMERIC = 'numeric';
const OTHER = 'other';
const OTHER_UPLOAD = 'other-upload';
const OTHER_UPLOAD_ATTACHMENT = 'other-attachment';

// SCORE TYPES
const SCALED = 'scaled';
const RAW = 'raw';
const MIN = 'min';
const MAX = 'max';

// ACTIVITY TYPES
const LESSON = 'http://adlnet.gov/expapi/activities/lesson';
const COURSE = 'http://adlnet.gov/expapi/activities/course';
const ASSESSMENT = 'http://adlnet.gov/expapi/activities/assessment';
const PAGE = 'https://w3id.org/xapi/acrossx/activities/page';
const PROFILE = 'http://adlnet.gov/expapi/activities/profile';
const SECTION = 'https://w3id.org/xapi/netc-e-learning/activity-types/section';

// EXTENSIONS
const SCHOOL_CENTER = 'https://w3id.org/xapi/netc/extensions/school-center';
const LAUNCH_LOCATION = 'https://w3id.org/xapi/navy/extensions/launch-location';
const USER_AGENT = 'https://w3id.org/xapi/netc/extensions/user-agent';
const RESPONSE_EXPLANATION = 'https://w3id.org/xapi/netc-assessment/extensions/result/response-explanation';
const RESPONSE_TYPE = 'https://w3id.org/xapi/netc-assessment/extensions/result/response-type';
const EXTENDED_INTERACTION_TYPE = 'https://w3id.org/xapi/netc-assessment/extensions/activity/extended-interaction-type';

// ERRORS
const INVALID_ACTIVITY_TYPE = 'Invalid activity type. Activity type must be either "course" or "lesson".'
const COURSE_INITIALIZE_SUCCEED_LESSON_ERROR = 'You must initialize a course before initializing a lesson.';
const NO_COURSE_REGISTRATION_ID_ERROR = 'No registration id for this course attempt.';
const NO_LESSON_REGISTRATION_ID_ERROR = 'No registration id is associated with this attempt. You must call initialize() before suspending an attempt.';
const ATTEMPT_TERMINATED_ERROR= "This attempt has been terminated and can't be resumed.";
const NO_SUSPENDED_DATA_ERROR = "There's no suspended data for this attempt.";
const COMPLETED_NOT_SET_ERROR = 'completed is undefined';
const COMPLETED_NOT_TYPE_BOOLEAN_ERROR = 'completed must be type boolean.';
const SUCCESS_NOT_SET_ERROR = 'success is undefined';
const SUCCESS_NOT_TYPE_BOOLEAN_ERROR = 'success must be type boolean.';
const TYPE_NOT_SET_ERROR = 'type is undefined';
const TYPE_NOT_TYPE_STRING_ERROR = 'type must be a string.';
const SCORE_NOT_SET_ERROR = 'score is undefined';
const SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR = 'score must be type string or number.';
const INTERACTION_TYPE_NOT_SET_ERROR = 'You must provided an "interactionType" value.';
const ID_NOT_SET_ERROR = 'You must provided an "id" value.';
const CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR = 'correctResponsesPattern has to be an array.';
const RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR = 'Response Pattern has to be an array.';
const CHOICES_NOT_TYPE_ARRAY_ERROR = 'choices has to be an array.';
const SCALE_NOT_TYPE_ARRAY_ERROR = 'scale has to be an array.';
const SOURCE_NOT_TYPE_ARRAY_ERROR = 'source has to be an array.';
const TARGET_NOT_TYPE_ARRAY_ERROR = 'target has to be an array.';
const STEPS_NOT_TYPE_ARRAY_ERROR = 'steps has to be an array.';
const RESPONSE_VALUE_UNDEFINED_ERROR = 'A response value is required.';
const INTERACTION_UNDEFINED_ERROR = 'The interaction argument is required.';
const INTERACTION_NOT_TYPE_OBJECT_ERROR = 'The interaction argument is not an object.';
const INTERACTION_ID_UNDEFINED_ERROR = 'The interaction property id is required.';
const INTERACTION_TYPE_UNDEFINED_ERROR = 'The interaction property interactionType is required.';
const PARENT_UNDEFINED_ERROR = 'The parent argument is required.';
const PARENT_NOT_TYPE_OBJECT_ERROR = 'The parent argument is not an object.';
const PARENT_ID_NOT_TYPE_STRING_ERROR = 'Parent id is not a string.'
const PARENT_NAME_NOT_TYPE_STRING_ERROR = 'Parent name is not a string';
const PARENT_DESCRIPTION_NOT_TYPE_STRING_ERROR = 'Parent description is not a string';
const ACTIVITY_ID_UNDEFINED_ERROR = 'Activity id required.';
const ACTIVITY_ID_NOT_TYPE_STRING_ERROR = 'Activity id is not a string.'
const ACTIVITY_NAME_UNDEFINED_ERROR = 'Activity name is required.';
const ACTIVITY_NAME_NOT_TYPE_STRING_ERROR = 'Activity name is not a string';
const ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR = 'Activity description is not a string';
const GROUP_ID_UNDEFINED_ERROR = 'Group id required.';
const GROUP_ID_NOT_TYPE_STRING_ERROR = 'Group id is not a string.'
const GROUP_NAME_UNDEFINED_ERROR = 'Group name is required.';
const GROUP_NAME_NOT_TYPE_STRING_ERROR = 'Group name is not a string';
const GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR = 'Group description is not a string';
const EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR = 'key must be either a string or an object.';
const EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR = 'value must be either a string or an object.';
const PARENT_ID_UNDEFINED_ERROR = 'parent id is required.';
const LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR = 'launch parameters or configuration object required.';
const ACTIVITY_AND_GROUP_OBJECT_REQUIRED_ERROR = 'You must create a statement by using setActivity() and setGroup().';
const ACTIVITY_OBJECT_REQUIRED_ERROR = 'An activity is required to be able to send statements. Use setActivity() to add an activity';
const GROUP_OBJECT_REQUIRED_ERROR = 'A group is required to be able to send statements. Use setGroup() to add a group';
const COURSE_IS_TERMINATED_ERROR = 'This course attempt has been terminated. Create a new course attempt by calling initialize().';
const LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR = 'This lesson attempt needs to be initialized before resuming. Create a new lesson attempt by calling initialize().';
const LESSON_ATTEMPT_TERMINATED = 'This lesson attempt has been terminated. Create a new lesson attempt by calling initialize().';
const LESSON_ATTEMPT_NOT_SUSPENDED_ERROR = 'This lesson attempt has not been suspended. Calling suspend() will suspend this lesson.';
const EMPTY_ACTIVITY_STATE_ERROR = 'You must call initialize on a course or lesson before terminating or resume from a suspended lesson.';
const LESSON_ATTEMPT_NOT_FOUND = 'Lesson attempt not found.';
const OTHER_RESPONSE_NOT_PROVIDED_ERROR = 'You must provide a value for "otherResponse" when using the "CHOICE_WITH_EXPLANATION" interaction type.';
const ATTACHMENT_NOT_PROVIDED_ERROR = 'You must provide an "attachment" value when using "OTHER_UPLOAD_ATTACHMENT interaction type';
const EXTENSION_KEY_NOT_PROVIDED_ERROR = 'You must provide a "key" value.';
const ATTACHMENT_PARAM_NOT_AN_ARRAY = 'You must provide an array for attachment value.';
const OTHER_RESPONSE_NOT_PROVIDED = 'You must provide a "otherResponse" value';
const INVALID_INTERACTION_TYPE_COMPONENT_LIST = "Invalid interactionType. This interaction type most likely doesn't need a component list."
const INVALID_MATCHING_PERFORMANCE_RESPONSE = 'matching and performance response pattern must be in the correct format.'
const GROUP_OBJECT_REQUIRED_TO_SUSPEND = 'A group is required to be able to suspend statements. Use setGroup() to add a group';
const GROUP_OBJECT_REQUIRED_TO_BOOKMARK = 'A group is required to be able to bookmark. Use setGroup() to add a group';

export const ERROR = {
    INVALID_ACTIVITY_TYPE,
    COURSE_INITIALIZE_SUCCEED_LESSON_ERROR,
    NO_COURSE_REGISTRATION_ID_ERROR,
    NO_LESSON_REGISTRATION_ID_ERROR,
    ATTEMPT_TERMINATED_ERROR,
    NO_SUSPENDED_DATA_ERROR,
    COMPLETED_NOT_SET_ERROR,
    COMPLETED_NOT_TYPE_BOOLEAN_ERROR,
    SUCCESS_NOT_SET_ERROR,
    SUCCESS_NOT_TYPE_BOOLEAN_ERROR,
    TYPE_NOT_SET_ERROR,
    TYPE_NOT_TYPE_STRING_ERROR,
    SCORE_NOT_SET_ERROR,
    SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR,
    INTERACTION_TYPE_NOT_SET_ERROR,
    ID_NOT_SET_ERROR,
    CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR,
    INVALID_INTERACTION_TYPE_COMPONENT_LIST,
    RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR,
    CHOICES_NOT_TYPE_ARRAY_ERROR,
    SCALE_NOT_TYPE_ARRAY_ERROR,
    SOURCE_NOT_TYPE_ARRAY_ERROR,
    TARGET_NOT_TYPE_ARRAY_ERROR,
    STEPS_NOT_TYPE_ARRAY_ERROR,
    RESPONSE_VALUE_UNDEFINED_ERROR,
    INTERACTION_UNDEFINED_ERROR,
    INTERACTION_NOT_TYPE_OBJECT_ERROR,
    INTERACTION_ID_UNDEFINED_ERROR,
    INTERACTION_TYPE_UNDEFINED_ERROR,
    PARENT_UNDEFINED_ERROR,
    PARENT_NOT_TYPE_OBJECT_ERROR,
    PARENT_ID_NOT_TYPE_STRING_ERROR,
    PARENT_NAME_NOT_TYPE_STRING_ERROR,
    PARENT_DESCRIPTION_NOT_TYPE_STRING_ERROR,
    ACTIVITY_ID_UNDEFINED_ERROR,
    ACTIVITY_ID_NOT_TYPE_STRING_ERROR,
    ACTIVITY_NAME_UNDEFINED_ERROR,
    ACTIVITY_NAME_NOT_TYPE_STRING_ERROR,
    ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR,
    GROUP_ID_UNDEFINED_ERROR,
    GROUP_ID_NOT_TYPE_STRING_ERROR,
    GROUP_NAME_UNDEFINED_ERROR,
    GROUP_NAME_NOT_TYPE_STRING_ERROR,
    GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR,
    EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR,
    EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR,
    PARENT_ID_UNDEFINED_ERROR,
    LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR,
    ACTIVITY_AND_GROUP_OBJECT_REQUIRED_ERROR,
    ACTIVITY_OBJECT_REQUIRED_ERROR,
    GROUP_OBJECT_REQUIRED_ERROR,
    COURSE_IS_TERMINATED_ERROR,
    LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR,
    LESSON_ATTEMPT_TERMINATED,
    LESSON_ATTEMPT_NOT_SUSPENDED_ERROR,
    EMPTY_ACTIVITY_STATE_ERROR,
    LESSON_ATTEMPT_NOT_FOUND,
    OTHER_RESPONSE_NOT_PROVIDED_ERROR,
    ATTACHMENT_NOT_PROVIDED_ERROR,
    EXTENSION_KEY_NOT_PROVIDED_ERROR,
    ATTACHMENT_PARAM_NOT_AN_ARRAY,
    OTHER_RESPONSE_NOT_PROVIDED,
    INVALID_MATCHING_PERFORMANCE_RESPONSE,
    GROUP_OBJECT_REQUIRED_TO_SUSPEND,
    GROUP_OBJECT_REQUIRED_TO_BOOKMARK
}

export const EXTENSIONS = {
    SCHOOL_CENTER,
    LAUNCH_LOCATION,
    USER_AGENT,
    RESPONSE_EXPLANATION,
    RESPONSE_TYPE,
    EXTENDED_INTERACTION_TYPE
}

export const VERB = {
    INITIALIZED,
    COMPLETED,
    PASSED,
    FAILED,
    TERMINATED,
    SUSPENDED,
    RESUMED,
    RESPONDED,
    SCORED,
    BOOKMARKED,
    VIEWED,
    ACCESSED,
    EXITED,
}
export const INTERACTION = {
    TRUE_FALSE,
    CHOICE,
    CHOICE_WITH_EXPLANATION,
    FILL_IN,
    LONG_FILL_IN,
    MATCHING,
    PERFORMANCE,
    SEQUENCING,
    LIKERT,
    NUMERIC,
    OTHER,
    OTHER_UPLOAD,
    OTHER_UPLOAD_ATTACHMENT
}

export type INTERACTION_TYPE = { INTERACTION };
export const SCORE = {
    SCALED,
    RAW,
    MIN,
    MAX
}
export const ACTIVITY_TYPES = {
    LESSON,
    COURSE,
    ASSESSMENT,
    SECTION,
    PROFILE,
    PAGE
}

export enum ACTIVITY {
    COURSE,
    LESSON,
    ASSESSMENT
}
