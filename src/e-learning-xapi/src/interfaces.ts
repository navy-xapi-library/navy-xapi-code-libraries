/** ***********************************************************************
*
* Veracity Technology Consultants CONFIDENTIAL
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Veracity Technology Consultants and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Veracity Technology Consultants
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Veracity Technology Consultants.
*/
export interface IInteractionComponentListItem {
  id: string,
  description: {
    [key: string]: string
  }
}
export interface INameAndDescription {
  language: string;
}
export interface InteractionComponent {
  id: string
  description: INameAndDescription;
}

export interface IDefinition {
  name?: Display;
  description?: Display;
  type: string;
}

export interface ActivityContext {
  activity: {
    id: String;
    definition?: IDefinition;
  };
  contextActivity: {
    id: String;
    definition?: IDefinition;
  };
}

export interface IConfiguration {
  endpoint: string;
  auth: string;
  actor: {
    name: string;
    account: {
      homePage: string;
      name: string;
    };
    objectType: string;
  };
  platform?: string;
  strictCallbacks?: boolean;
}

export interface IAssessmentParent {
  id: string;
  definition: IDefinition;
}
export interface InteractionDefinition {
  name?: {
    en: string;
  };
  description?: {
    en: string;
  };
  correctResponsesPattern?: Array<string>;
  choices?: InteractionComponent[];
  scale?: InteractionComponent[];
  source?: InteractionComponent[];
  target?: InteractionComponent[];
  steps?: InteractionComponent[];
}
export interface Interaction {
  id: string;
  interactionType: string;
  definition?: InteractionDefinition;
}

export interface Result {
  error?: string;
  data?: any;
}
export interface Response {
  status: any;
  response: any;
}

export interface IAttemptResults {
  duration?: string,
  completion?: boolean,
  success?: boolean,
  score?: {
    scaled?: number,
    raw?: number,
    min?: number,
    max?: number,
  }
}
export interface ILessonAttempt {
  registration: string,
  initialized?: boolean,
  terminated?: boolean,
  suspended?: boolean,
  resumed?: boolean,
  bookmark?: string,
  result?: IAttemptResults
}
export interface ILesson {
  [key: string]: Array<ILessonAttempt>
}
export interface ICourseAttempt {
  registration: string,
  initialized?: boolean,
  terminated: boolean,
  lastSuspendedAttempt: string,
  lessons?: ILesson,
  bookmark?: string,
}
export interface IStandAloneLessonAttempt {
  attempts: Array<ILessonAttempt>
}

export interface IAttachment {
  type: {
    usageType: string;
    display: {
      en: string
    };
    description?: {
      en: string
    };
    contentType: string;
  };
  value: string;
}
type MatchingType = {
  target: string,
  source: string
}
type PerformanceType = {
  id: string,
  response: string
}
type NumericType = {
  max?: number,
  min?: number
}
export type InteractionComponentList = {
  source: [];
  target: [];
} | []


export interface IAccount {
  homePage: string,
  name: string
}
export interface IActor {
  name: string,
  objectType: string,
  account: IAccount
}
export type Display = {
  [key: string] : string
}
export interface IVerb {
  id: string,
  display: Display
}
export interface IObject {
  id: string,
  definition: IDefinition
}
export interface IDefinition {
  name?: Display;
  description?: Display;
  type: string;
  interactionType?: string;
  correctResponsePattern?: string[]
}
export interface ICategory {
  id: string;
  definition: {
    type: string
  }
}
export interface IGrouping {
  id: string,
  definition: {
    name?: Display,
    description?: Display
    type: string
  }
}
export interface IContextActivities {
  category: ICategory[],
  grouping: IGrouping[]
}
export interface IExtension { [key: string]: any };
export interface IContext {
  contextActivities: IContextActivities,
  registration: string,
  extensions: IExtension
  platform?: string
}
export interface IStatement {
  actor: IActor,
  verb: IVerb,
  object: IObject,
  context: IContext,
  id: string,
  timestamp: string,
}
export interface IStatementConfiguration {
  actor: IActor;
  registration: string;
  object: IObject;
  group: IGrouping;
  parent?: IAssessmentParent;
  result?: IAttemptResults;
  interaction?: Interaction;
  platform?: string;
  granular?: boolean;
  extensions?: IExtension;
  isCMI5?: boolean;
}

type TrueFalse = boolean[];
type Choice = string[];
type fillIn = string[];
type longFillIn = string[];
type likert = string[];
type matching = MatchingType[];
type performance = PerformanceType[];
type sequencing = number[];
type numeric = (NumericType | number)[]
type other = string[];
