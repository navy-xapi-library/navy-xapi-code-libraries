// constants
import { INTERACTION, VERB } from "../constants/constants";
// has scorm profile category
// initialized, suspend, resume,
// complete, score, passed, failed,
// terminated, responded
// only initialized and terminated course doesn't use the scorm profile

export const isObject = o => o?.constructor === Object;

export const getVerbID = (verb) => {
  switch (verb) {
    case VERB.INITIALIZED:
    case VERB.COMPLETED:
    case VERB.PASSED:
    case VERB.FAILED:
    case VERB.TERMINATED:
    case VERB.SUSPENDED:
    case VERB.RESUMED:
    case VERB.RESPONDED:
    case VERB.SCORED:
    case VERB.BOOKMARKED:
      return `http://adlnet.gov/expapi/verbs/${verb}`;
    case VERB.VIEWED:
      return `http://id.tincanapi.com/verb/${verb}`;
    case VERB.ACCESSED:
      return `https://w3id.org/xapi/performance-support/verbs/${verb}`;
    default:
      return `http://adlnet.gov/expapi/verbs/${verb}`;
  }
};

export const parseLaunchParams = (parsedURL, baseconf?:any) => {
  const sp = parsedURL.search;
  const params = new URLSearchParams(sp);
  if(!baseconf){
    baseconf = {};
  }
  if (params.get("endpoint")) {
    baseconf["endpoint"] = params.get("endpoint");
  }
  if (params.get("auth")) {
    baseconf["auth"] = params.get("auth");
  }
  if (params.get("actor")) {
    baseconf["actor"] = params.get("actor");
  }
  if (params.get("registration")) {
    baseconf["registration"] = params.get("registration");
  }

  return baseconf;
};

export const ruuid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};
