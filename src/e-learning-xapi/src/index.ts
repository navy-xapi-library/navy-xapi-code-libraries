/** ***********************************************************************
 *
 * Veracity Technology Consultants CONFIDENTIAL
 * __________________
 *
 *  2019 Veracity Technology Consultants
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Veracity Technology Consultants and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Veracity Technology Consultants
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Veracity Technology Consultants.
 */
// @ts-ignore
import _ADL from '../../xapiWrapper/ADL';

import { generateStatement } from './statements/statementBuilder';

// constants
import {
  VERB,
  INTERACTION,
  SCORE,
  ACTIVITY_TYPES,
  ACTIVITY,
  EXTENSIONS,
  ERROR,
} from './constants/constants';

//utils
// @ts-ignore
import {
  parseLaunchParams,
  ruuid,
  isObject,
} from './utils/utils';

// interfaces
import {
  IConfiguration,
  Interaction,
  IAssessmentParent,
  Result,
  IStatement,
  ICourseAttempt,
  ILessonAttempt,
  IAttachment,
  IExtension,
  InteractionComponentList,
  IInteractionComponentListItem,
  IStatementConfiguration
} from './interfaces';

export class ELearningXAPI {
  private _ADL: any = null;

  private _config: IConfiguration | null = null;

  private _lessonRegistration: string | null;
  private _courseRegistration: string | null;

  // statement object
  private _statementObject: Partial<IStatementConfiguration> = {};
  private _statement: Partial<IStatement> = {};

  private _extensions: IExtension = {};

  private _activityID: string | null = null;

  private _platform: string = '';

  private _activityState: ICourseAttempt | ILessonAttempt
  private _currentLessonAttempt: ILessonAttempt | null; 

  /**
   * @constructor
   * @param {Configuration} config - A configuration object that contains auth credentials to an lrs endpoint
   */
  constructor(config?: IConfiguration) {
    if (config) {
      this._config = { ...config, strictCallbacks: true };
      this._platform = config.platform ? config.platform : '';
      this._statementObject.actor = config.actor;
    }

    this._ADL = _ADL();
    this._lessonRegistration = null;
    this._activityState = <ICourseAttempt>{};
    this._currentLessonAttempt = null;
    this._courseRegistration = null;
  }

  /**
   * Store initialize value to true for a Course or lesson attempt activity state and send an initialized statement to the configured LRS.
   * @param {string} activityType
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public initialize = async (activityType: ACTIVITY): Promise<Result> => {
    const { object, group } = this._statementObject;
    const { COURSE, LESSON } = ACTIVITY_TYPES;
    const isCourse = activityType === ACTIVITY.COURSE;
    const registration: string = ruuid();

    if (!object) throw Error(ERROR.ACTIVITY_OBJECT_REQUIRED_ERROR)
    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_ERROR)

    let stmtClone = { ...this._statementObject };
    if (isCourse) {
      stmtClone = { ...stmtClone, object: stmtClone.group, group: undefined }
      this.courseRegistration = registration;
      this._activityState = this.createCourseAttemptState({ registration, initialized: true });
    } else {
      const state = await this.getActivityState(group.id)
        .catch(error => Promise.reject({ error }))

      const initLessonAttempt = this.createLessonAttemptState({ registration, initialized: true });
      this._activityState = this.addLessonAttempt(state as ICourseAttempt, object.id, initLessonAttempt);
      this.lessonRegistration = registration;
      this._currentLessonAttempt = initLessonAttempt;
    }

    await this.setActivityState(group.id, this._activityState as ICourseAttempt, null)
      .catch(error => Promise.reject({ error }))

    let stmt: IStatement = generateStatement(
      {
        ...stmtClone,
        registration,
        verb: VERB.INITIALIZED,
        activityType: isCourse ? COURSE : LESSON,
        isScorm: stmtClone.group ? true : false,
        platform: this._platform ? this._platform : undefined,
      }
    )

    return this.sendStatement(stmt);
  };

  /**
   * Suspend an attempt using activity state and send a suspend statement to the configured LRS.
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */

  public suspend = async (activityId?: string): Promise<Result> => {
    const { object, group } = this._statementObject;
    const { COURSE, LESSON } = ACTIVITY;

    if (!object) throw Error(ERROR.ACTIVITY_OBJECT_REQUIRED_ERROR)
    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_TO_SUSPEND)

    if (this._activityState.terminated) throw Error(ERROR.COURSE_IS_TERMINATED_ERROR);

    if (activityId) {
      if (!this._activityState["lessons"][activityId]) throw Error(ERROR.LESSON_ATTEMPT_NOT_FOUND);

      const length = this._activityState["lessons"][activityId].length;
      const attempt = this._activityState["lessons"][activityId][length - 1]
      this._currentLessonAttempt = attempt;
      this.lessonRegistration = attempt.registration;
    }

    this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt as ILessonAttempt, { suspended: true });
    this._activityState = this.updateCourseActivityState(this._activityState as ICourseAttempt, LESSON, this._currentLessonAttempt);
    this._activityState = this.updateCourseActivityState(this._activityState as ICourseAttempt, COURSE, { lastSuspendedAttempt: object })

    await this.setActivityState(group.id, this._activityState, null)
      .catch(error => Promise.reject({ error }));

    let stmt: IStatement = generateStatement(
      {
        ...this._statementObject,
        registration: this.lessonRegistration,
        verb: VERB.SUSPENDED,
        activityType: ACTIVITY_TYPES.LESSON,
        isScorm: group ? true : false,
        platform: this._platform ? this._platform : undefined,
      }
    )

    return this.sendStatement(stmt);
  };

  /**
   * Resume an attempt using stored activity state or passed in activityId and send a resume statement to the configured LRS.
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */

  public resume = async (activityId?: string): Promise<Result> => {
    let lesson;

    const { group } = this._statementObject;

    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_ERROR)

    const state: any = await this.getActivityState(group.id).catch(error => Promise.reject({ error }))

    this._activityState = { ...state };

    if (state.terminated) throw Error(ERROR.COURSE_IS_TERMINATED_ERROR);

    if (activityId) {
      if (!state.lessons[activityId]) {
        throw Error(ERROR.LESSON_ATTEMPT_NOT_INITIALIZED_BEFORE_RESUME_ERROR)
      }
      lesson = activityId
    } else if (state.lastSuspendedAttempt === '') {
      throw Error('No Attempts have been suspended')
    } else {
      lesson = state.lastSuspendedAttempt.id
      this.setActivity(
        state.lastSuspendedAttempt.id,
        state.lastSuspendedAttempt.definition.name.en,
        state.lastSuspendedAttempt.definition.description.en
      )
    }
    // looks like this lesson has been attempted, so lets see if it has been terminated 
    this._currentLessonAttempt = this._activityState['lessons'][lesson][
      state.lessons[lesson].length - 1
    ];

    if (this._currentLessonAttempt?.terminated) {
      throw Error(ERROR.LESSON_ATTEMPT_TERMINATED);
    }
    if (!this._currentLessonAttempt?.suspended) {
      throw Error(ERROR.LESSON_ATTEMPT_NOT_SUSPENDED_ERROR);
    }

    this.courseRegistration = state.registration;
    this.lessonRegistration = this._currentLessonAttempt?.registration as string;

    let stmt: IStatement = generateStatement(
      {
        ...this._statementObject,
        registration: this.lessonRegistration,
        verb: VERB.RESUMED,
        activityType: ACTIVITY_TYPES.LESSON,
        isScorm: this._statementObject.group ? true : false,
        platform: this._platform ? this._platform : undefined,
      }
    )
    return this.sendStatement(stmt);
  };
  /**
   * Set terminated value to true for a Course or Lesson attempt activity state and send an terminated statement to the configured LRS.
   * @param {string} activityType
   * @param {string} duration
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public terminate = async (activityType: ACTIVITY, duration?: string): Promise<Result> => {
    const { object, group } = this._statementObject;
    const isCourse = activityType === ACTIVITY.COURSE;

    if (!object) throw Error(ERROR.ACTIVITY_OBJECT_REQUIRED_ERROR)
    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_TO_SUSPEND)

    if (!this._activityState) throw Error(ERROR.EMPTY_ACTIVITY_STATE_ERROR);

    let stmtClone = { ...this._statementObject }

    if (isCourse) {
      this._activityState = this.updateCourseActivityState(
        this._activityState as ICourseAttempt,
        ACTIVITY.COURSE,
        { terminated: true }
      );

      await this.setActivityState(group.id, this._activityState, null)
        .catch(error => Promise.reject({ error }));

      stmtClone = { ...stmtClone, object: stmtClone.group, group: undefined, parent: undefined }
    } else {
      this._currentLessonAttempt = this.updateLessonActivityState(this._currentLessonAttempt as ICourseAttempt, { terminated: true });
      this._activityState = this.updateCourseActivityState(this._activityState as ICourseAttempt, ACTIVITY.LESSON, this._currentLessonAttempt);

      await this.setActivityState(group.id, this._activityState, null)
        .catch(error => Promise.reject({ error }));
    }

    let stmt: IStatement = generateStatement(
      {
        ...stmtClone,
        verb: VERB.TERMINATED,
        registration: isCourse ? this._courseRegistration : this._lessonRegistration,
        activity: isCourse ? group : undefined,
        activityType: isCourse ? ACTIVITY_TYPES.COURSE : ACTIVITY_TYPES.LESSON,
        result: isCourse ? undefined : { ...this._currentLessonAttempt?.result, duration }
      }
    )

    isCourse ? this._courseRegistration = null : this._lessonRegistration = null;

    return this.sendStatement(stmt);
  };

  /**
   * Sends completion statement to the configured LRS.
   * @param {boolean} completed - A boolean value representing if a user has completed an activity
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public setComplete = (completed: boolean): Promise<Result> | Result => {

    if (typeof completed === 'undefined') {
      throw Error(ERROR.COMPLETED_NOT_SET_ERROR);
    }

    if (typeof completed !== 'boolean') {
      throw TypeError(ERROR.COMPLETED_NOT_TYPE_BOOLEAN_ERROR);
    }

    this._currentLessonAttempt = this.updateLessonActivityState(
      this._currentLessonAttempt as ILessonAttempt,
      {
        result: { completion: completed },
      }
    );

    // save completion boolean to be used in terminate/end attempt result
    this.updateCourseActivityState(this._activityState as ICourseAttempt, ACTIVITY.LESSON, this._currentLessonAttempt);

    let stmt: IStatement = generateStatement(
      {
        ...this._statementObject,
        verb: VERB.COMPLETED,
        registration: this._lessonRegistration,
        activityType: ACTIVITY_TYPES.LESSON,
        isScorm: this._statementObject.group ? true : false,
        platform: this._platform ? this._platform : undefined,
      }
    );
    return this.sendStatement(stmt);
  };

  /**
   * Sends success statement to the configured LRS.
   * @param {boolean} completed - A boolean value representing if a user pass or fail a success criteria.
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public setSuccess = (success: boolean): Promise<Result> | Result => {
    if (typeof success === 'undefined') {
      throw Error(ERROR.SUCCESS_NOT_SET_ERROR);
    }

    if (typeof success !== 'boolean') {
      throw TypeError(ERROR.SUCCESS_NOT_TYPE_BOOLEAN_ERROR);
    }
    // update current lesson state
    this._currentLessonAttempt = this.updateLessonActivityState(
      this._currentLessonAttempt as ILessonAttempt,
      {
        result: { success: success },
      }
    );

    // update activity state with success property and value
    this.updateCourseActivityState(
      this._activityState as ICourseAttempt,
      ACTIVITY.LESSON,
      this._currentLessonAttempt
    );
    let stmt: IStatement = generateStatement(
      {
        ...this._statementObject,
        verb: success ? VERB.PASSED : VERB.FAILED,
        registration: this._lessonRegistration,
        activityType: ACTIVITY_TYPES.COURSE,
        isScorm: this._statementObject.group ? true : false,
        platform: this._platform ? this._platform : undefined,
      }
    )

    return this.sendStatement(stmt);
  };
  /**
   * Sends success statement to the configured LRS.
   * @param {string} type - The score type. For instance, scaled, raw, min, or max.
   * @param {string} score - The learner's score value.
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public setScore = (type: string, score: number): Promise<Result> | Result => {
    if (typeof type !== 'string' && typeof score === 'undefined') {
      throw TypeError(ERROR.TYPE_NOT_TYPE_STRING_ERROR);
    }

    if (typeof score !== 'number' && typeof score === 'undefined') {
      throw TypeError(`score ${ERROR.SCORE_NOT_TYPE_STRING_OR_NUMBER_ERROR}`);
    }

    if (type === SCORE.SCALED && score > 1) {
      throw Error('Scaled value must have a maximum value of 1');
    }
    // update current lesson state
    this._currentLessonAttempt = this.updateLessonActivityState(
      this._currentLessonAttempt as ILessonAttempt,
      {
        result: { score: { [type]: score } },
      }
    );

    // update activity state with success property and value
    this.updateCourseActivityState(
      this._activityState as ICourseAttempt,
      ACTIVITY.LESSON,
      this._currentLessonAttempt
    );

    if (type === SCORE.SCALED || type === SCORE.RAW) {
      let stmt: IStatement = generateStatement(
        {
          ...this._statementObject,
          verb: VERB.SCORED,
          registration: this._lessonRegistration,
          activityType: ACTIVITY_TYPES.LESSON,
          result: { score: { [type]: score } },
          isScorm: this._statementObject.group ? true : false,
          platform: this._platform ? this._platform : undefined,
        }
      )
      return this.sendStatement(stmt);
    }
    return Promise.resolve({});
  };

  /**
   * Set the bookmark property to the location value within the configured LRS activity state
   * @param {string} location - A string describing a location.
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public setBookmark = async (location: string): Promise<Result> => {
    const { group } = this._statementObject;

    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_TO_BOOKMARK)

    this._activityState = this.updateCourseActivityState(
      this._activityState as ICourseAttempt,
      ACTIVITY.COURSE,
      { bookmark: location }
    );
    return this.setActivityState(group.id, this._activityState, null);
  };

  /**
   * Returns the bookmark property value within the activity state
   * @returns {string}
   */
  public getBookmark = () => {
    return this._activityState?.bookmark;
  };

  /**
   * Creates an interaction definition object to be used within the statement "object" property body
   * @param {string} interactionType - A predefined String value for the type of interaction.
   * @param {string} id - A unique identifier for the interaction according to the Activity ID requirements in the NETC Common Reference Profile
   * @param {string} name - A value that represents the official name or title of the interaction activity.
   * @param {string} description - A value that represents a short description of the interaction activity.
   * @param {Array} correctResponsesPattern - An array of patterns representing the correct response to the interaction.
   * @param {Array} interactionComponentList - An array list of interaction component(s) list or an object with a source and target property which values are arrays of interaction component list for a matching interaction type.
   * @returns {Object <Interaction>}
   */

  public createInteraction = (interactionType: string, id: string, name: string, description: string, correctResponsesPattern: [], interactionComponentList: InteractionComponentList): Interaction => {
    if (!interactionType) {
      throw Error(ERROR.INTERACTION_TYPE_NOT_SET_ERROR);
    }
    // id is required, but name and description are optional
    if (!id) {
      throw Error(ERROR.ID_NOT_SET_ERROR);
    }

    // lets build the interactionObject
    let interactionObject: Interaction = {
      id,
      interactionType,
    };

    // if a name was provided add it to the interactionObject
    if (name) {
      interactionObject.definition = {
        ...interactionObject.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the interactionObject
    if (description) {
      interactionObject.definition = {
        ...interactionObject.definition,
        description: {
          en: description,
        },
      };
    }
    // if definition then there must be a name or description value that created the definition property
    if (interactionObject.definition) {
      if (correctResponsesPattern) {
        if (!Array.isArray(correctResponsesPattern)) {
          throw TypeError(ERROR.CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR);
        }
      }

      if (interactionComponentList) {
        if (
          interactionType !== INTERACTION.MATCHING &&
          !Array.isArray(interactionComponentList)
        ) {
          throw TypeError(ERROR.CORRECT_RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR);
        } else if (
          interactionType === INTERACTION.MATCHING &&
          !isObject(interactionComponentList)
        ) {
          throw TypeError('Matching response pattern must be an object');
        }
      }
      // note : property won't set if it's undefined
      // ternary operator used to catch passed in null values
      interactionObject.definition = {
        ...interactionObject.definition,
        correctResponsesPattern: correctResponsesPattern,
        choices:
          interactionType === INTERACTION.CHOICE ||
            interactionType === INTERACTION.CHOICE_WITH_EXPLANATION ||
            interactionType === INTERACTION.SEQUENCING
            ? (interactionComponentList as [])
            : undefined,
        scale:
          interactionType === INTERACTION.LIKERT
            ? (interactionComponentList as [])
            : undefined,
        source:
          interactionType === INTERACTION.MATCHING
            ? (interactionComponentList as {})['source']
            : undefined,
        target:
          interactionType === INTERACTION.MATCHING
            ? (interactionComponentList as [])['target']
            : undefined,
        steps:
          interactionType === INTERACTION.PERFORMANCE
            ? (interactionComponentList as [])
            : undefined,
      };
    }

    return interactionObject;
  };

  /**
   * Sends interaction response to the configured LRS.
   * @param {Array <strings} response - An array that holds the response(s) of an interaction.
   * @param {Object <Interaction>} interaction - The interaction object defining the interaction activity
   * @param {Object <AssessmentParent>} parent - The parent configuration object.
   * @param {string} otherResponse - A provided extra response if an interaction has multiple responses
   * @param {Array <IAttachment>} attachments - An array of attachment objects
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public sendInteraction = (response: string[], interaction: Interaction, parent: IAssessmentParent, otherResponse: string = '', attachments: IAttachment[]): Promise<Result> | Result => {

    if (!response) {
      throw Error(ERROR.RESPONSE_VALUE_UNDEFINED_ERROR);
    }
    if (!interaction) {
      throw Error(ERROR.INTERACTION_UNDEFINED_ERROR);
    }
    if (interaction) {
      if (typeof interaction !== 'object') {
        throw Error(ERROR.INTERACTION_NOT_TYPE_OBJECT_ERROR);
      }
      if (!interaction.id) {
        throw Error(ERROR.INTERACTION_ID_UNDEFINED_ERROR);
      }
      if (!interaction.interactionType) {
        throw Error(ERROR.INTERACTION_TYPE_UNDEFINED_ERROR);
      }
    }

    if (!parent) {
      throw Error(ERROR.PARENT_UNDEFINED_ERROR);
    }
    if (parent) {
      if (typeof parent !== 'object') {
        throw Error(ERROR.PARENT_NOT_TYPE_OBJECT_ERROR);
      }
    }

    const { interactionType } = interaction;

    if (
      interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT &&
      !attachments
    ) {
      throw Error(ERROR.ATTACHMENT_NOT_PROVIDED_ERROR);
    }

    if (
      interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT &&
      attachments.length < 1
    ) {
      throw Error(ERROR.ATTACHMENT_NOT_PROVIDED_ERROR);
    }

    if (!parent.hasOwnProperty('type')) {
      // defaults to
      if (parent.definition) {
        parent.definition.type =
          'http://adlnet.gov/expapi/activities/assessment';
      }
    }

    let result_extensions;
    let activity_extensions;

    if (interactionType === INTERACTION.CHOICE_WITH_EXPLANATION) {
      interaction.interactionType = INTERACTION.CHOICE;
      result_extensions = { [EXTENSIONS.RESPONSE_EXPLANATION]: otherResponse };
    }
    if (
      interactionType === INTERACTION.OTHER_UPLOAD ||
      interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT
    ) {
      interaction.interactionType = INTERACTION.OTHER;
      activity_extensions = { [EXTENSIONS.EXTENDED_INTERACTION_TYPE]: 'upload' };
    }
    if (interactionType === INTERACTION.OTHER_UPLOAD_ATTACHMENT) {
      if (!Array.isArray(attachments)) {
        throw Error(ERROR.ATTACHMENT_PARAM_NOT_AN_ARRAY);
      }
    }

    let stmt: IStatement = generateStatement(
      {
        ...this._statementObject,
        activity_extensions,
        verb: VERB.RESPONDED,
        objectType: ACTIVITY_TYPES.LESSON,
        interaction,
        parent,
        registration: this.lessonRegistration,
        result: { response: response[0].toString(), extensions:result_extensions },
        isScorm: this._statementObject.group ? true : false,
        platform: this._platform ? this._platform : undefined,
      }
    )

    return this.sendStatement(stmt, attachments);
  };

  /**
   * Send a "viewed" statement to the configured LRS.
   * @param {string} id
   * @param {string} name
   * @param {string} description
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public viewed = (id: string, name: string, description: string): Promise<Result> => {

    if (!id) {
      throw Error(ERROR.ID_NOT_SET_ERROR);
    }

    let object = {
      id,
      definition: {
        name: { en: name },
        description: { en: description }
      },
    }

    let stmtClone = { ...this._statementObject, granular: this._statementObject.object, object };

    let stmt: IStatement = generateStatement(
      {
        ...stmtClone,
        verb: VERB.VIEWED,
        registration: this._lessonRegistration,
        activityType: ACTIVITY_TYPES.PAGE,
        isScorm: false,
      }
    )

    return this.sendStatement(stmt);
  };
  /**
   * Send a "section" statement to the configured LRS.
   * @param {string} id
   * @param {string} name
   * @param {string} description
   * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
   */
  public section = (id: string, name: string, description: string): Promise<Result> => {
    const { object, group } = this._statementObject;

    if (!id) {
      throw Error(ERROR.ID_NOT_SET_ERROR);
    }

    if (!object) throw Error(ERROR.ACTIVITY_OBJECT_REQUIRED_ERROR)
    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_TO_SUSPEND)

    let updatedObject = {
      id,
      definition: {
        name: { en: name },
        description: { en: description }
      },
    }

    let stmtClone = { ...this._statementObject, granular: this._statementObject.object, object: updatedObject };

    let stmt: IStatement = generateStatement(
      {
        ...stmtClone,
        verb: VERB.COMPLETED,
        registration: this._lessonRegistration,
        activityType: ACTIVITY_TYPES.SECTION,
        isScorm: false,
      }
    )

    return this.sendStatement(stmt);
  };
  /**
 * Send a "exited" statement to the configured LRS.
 * @param {string} id
 * @param {string} name
 * @param {string} description
 * @returns {Promise<Result>} - On success the promise will resolve an empty object else reject with a user-defined exception or HTTP error code.
 */
  public exit = (id: string, name: string, description: string): Promise<Result> => {
    const { object, group } = this._statementObject;

    if (!id) {
      throw Error(ERROR.ID_NOT_SET_ERROR);
    }

    if (!object) throw Error(ERROR.ACTIVITY_OBJECT_REQUIRED_ERROR)
    if (!group) throw Error(ERROR.GROUP_OBJECT_REQUIRED_TO_SUSPEND)

    let updatedObject = {
      id,
      definition: {
        name: { en: name },
        description: { en: description }
      },
    }

    let stmtClone = { ...this._statementObject, granular: this._statementObject.object, object: updatedObject };

    let stmt: IStatement = generateStatement(
      {
        ...stmtClone,
        verb: VERB.EXITED,
        registration: this._lessonRegistration,
        activityType: ACTIVITY_TYPES.SECTION,
        isScorm: false,
      }
    )

    return this.sendStatement(stmt);
  }

  /**
   * Sets activity definition to the statement object
   * @param {string} id
   * @param {string} name
   * @param {string} description
   */
  public setActivity(id: string, name?: string, description?: string): void {
    // id is required, but name and description are optional
    if (!id) {
      throw Error(ERROR.ACTIVITY_ID_UNDEFINED_ERROR);
    }
    if (typeof id !== 'string') {
      throw TypeError(ERROR.ACTIVITY_ID_NOT_TYPE_STRING_ERROR);
    }

    let stmt: Partial<IStatementConfiguration> = { ...this._statementObject };
    stmt.object = { id: this._activityID ? this._activityID : id, definition: { type: '' } };

    // if a name was provided add it to the activity object
    if (name) {
      if (typeof name !== 'string') {
        throw TypeError(ERROR.ACTIVITY_NAME_NOT_TYPE_STRING_ERROR);
      }
      stmt.object.definition = {
        ...stmt.object.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      if (typeof description !== 'string') {
        throw TypeError(ERROR.ACTIVITY_DESCRIPTION_NOT_TYPE_STRING_ERROR);
      }
      stmt.object.definition = {
        ...stmt.object.definition,
        description: {
          en: description,
        },
      };
    }

    this._statementObject = stmt;
  }
  /**
   * Sets parent definition to the statement object
   * @param {string} id - A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile
   * @param {string} name - A value that represents  the official name or title of the Activity
   * @param {string} description - A value that represents a short description of the Activity
   */
  public setParent(id: string, name: string, description: string, type: string): void {
    // id is required, but name and description are optional
    if (!id) {
      throw Error(ERROR.PARENT_ID_UNDEFINED_ERROR);
    }
    if (typeof id !== 'string') {
      throw TypeError(ERROR.PARENT_ID_NOT_TYPE_STRING_ERROR);
    }

    let stmt: Partial<IStatementConfiguration> = { ...this._statementObject };
    stmt.parent = { id, definition: { type } };

    // if a name was provided add it to the activity object
    if (name) {
      if (typeof name !== 'string') {
        throw TypeError(ERROR.PARENT_NAME_NOT_TYPE_STRING_ERROR);
      }
      stmt.parent.definition = {
        ...stmt.parent.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      if (typeof description !== 'string') {
        throw TypeError(ERROR.PARENT_DESCRIPTION_NOT_TYPE_STRING_ERROR);
      }
      stmt.parent.definition = {
        ...stmt.parent.definition,
        description: {
          en: description,
        },
      };
    }

    this._statementObject = stmt;
  }
  /**
   * Sets group definition to the statement object
   * @param {string} id - A unique identifier for the interaction activity according to the Activity ID requirements in the NETC Common Reference Profile
   * @param {string} name - A value that represents  the official name or title of the Activity
   * @param {string} description - A value that represents a short description of the Activity
   */
  public setGroup(id: string, name: string, description: string): void {
    // id is required, but name and description are optional
    if (!id) {
      throw Error(ERROR.GROUP_ID_UNDEFINED_ERROR);
    }
    if (typeof id !== 'string') {
      throw TypeError(ERROR.GROUP_ID_NOT_TYPE_STRING_ERROR);
    }

    let stmt: Partial<IStatementConfiguration> = { ...this._statementObject };
    stmt.group = { id, definition: { type: '' } };

    // if a name was provided add it to the activity object
    if (name) {
      if (typeof name !== 'string') {
        throw TypeError(ERROR.GROUP_NAME_NOT_TYPE_STRING_ERROR);
      }
      stmt.group.definition = {
        ...stmt.group.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      if (typeof description !== 'string') {
        throw TypeError(ERROR.GROUP_DESCRIPTION_NOT_TYPE_STRING_ERROR);
      }
      stmt.group.definition = {
        ...stmt.group.definition,
        description: {
          en: description,
        },
      };
    }

    this._statementObject = stmt;
  }

  /**
   * Adds a single extension or an object of extensions to the statement object
   * @param {string} key - string identifer of the extension
   * @param {string} value - value of the extension
   */
  public addExtensions(key: string | {}, value?: string): void {
    // id is required
    if (!key) {
      throw Error(ERROR.EXTENSION_KEY_NOT_PROVIDED_ERROR);
    }
    if (typeof key !== 'string' && typeof key !== 'object') {
      throw TypeError(ERROR.EXTENSION_KEY_NOT_TYPE_STRING_OR_OBJECT_ERROR);
    }

    if (typeof key === 'string') {
      if (!value) {
        throw Error(ERROR.EXTENSION_VALUE_NOT_TYPE_STRING_OR_OBJECT_ERROR);
      }
      this._extensions[key] = value;
    }

    if (typeof key === 'object') {
      // spread the old and new extensions
      this._extensions = { ...this._extensions, ...key };
    }

    let stmt: Partial<IStatementConfiguration> = {
      ...this._statementObject,
      extensions: this._extensions,
    };

    this._statementObject = stmt;
  }

  public createParent = (id: string, name?: string, description?: string): IAssessmentParent => {
    if (!id) {
      throw Error(ERROR.PARENT_ID_UNDEFINED_ERROR);
    }

    let stmt: IAssessmentParent = { id, definition: { type: '' } };

    // if a name was provided add it to the activity object
    if (name) {
      stmt.definition = {
        ...stmt.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      stmt.definition = {
        ...stmt.definition,
        description: {
          en: description,
        },
      };
    }

    return stmt;
  };
  public createInteractionComponentList = (id: string, description: string): IInteractionComponentListItem => {
    if (typeof id !== 'string' && typeof description !== 'string')
      throw Error(ERROR.TYPE_NOT_TYPE_STRING_ERROR);

    return {
      id,
      description: { en: description },
    };
  };
  public createResponsePattern = (interactionType: string, response: string[]): string[] => {
    if (!Array.isArray(response))
      throw Error(ERROR.RESPONSE_PATTERN_NOT_AN_ARRAY_ERROR);
    return this.formatResponsePattern(interactionType, response);
  };

  private formatResponsePattern = (interactionType: string, response: string[]): string[] => {
    let crp;

    if (response.length === 1) {
      if (interactionType === INTERACTION.NUMERIC) {
        const split = response[0].split(':');
        return split.length === 1 ? response : [`${split[0]}[:]${split[1]}`];
      } else {
        return response;
      }
    }

    crp = response.reduce((accumulator, current, index) => {
      if (
        interactionType === INTERACTION.MATCHING ||
        interactionType === INTERACTION.PERFORMANCE
      ) {
        const split = current.split(':');

        if (split.length === 1)
          throw Error(ERROR.INVALID_MATCHING_PERFORMANCE_RESPONSE);

        accumulator += split[0] + '[.]' + split[1];
      } else {
        accumulator += current;
      }
      if (index < response.length - 1) {
        accumulator += '[,]';
      }
      return accumulator;
    }, '');

    return [crp];
  };

  public attachment = (value: any, usageType: string, contentType: string, display: string, description: string): IAttachment => {
    if (!value) {
      throw new Error('You must provide a value.');
    }
    if (!usageType && typeof usageType !== 'string') {
      throw new TypeError(`usageType ${ERROR.TYPE_NOT_TYPE_STRING_ERROR}`);
    }
    if (!contentType) {
      throw new Error(`contentType ${ERROR.TYPE_NOT_TYPE_STRING_ERROR}`);
    }
    if (!display) {
      throw new Error(`display ${ERROR.TYPE_NOT_TYPE_STRING_ERROR}`);
    }
    // build attachment array
    return {
      type: {
        usageType: this.getUsageType(usageType),
        display: { en: display },
        description: description ? { en: description } : undefined, // undefined won't set the property
        contentType,
      },
      value,
    };
  };
  private getUsageType = (type: string): string => {
    let types: IExtension = {};
    types['image'] = 'http://adlnet.gov/expapi/attachments/image';
    types['signature'] = 'http://adlnet.gov/expapi/attachments/signature';
    types['certification'] =
      'http://id.tincanapi.com/attachment/certificate-of-completion';

    return types[type];
  };

  public getLessonRegistration = (): string => {
    return this.lessonRegistration;
  };
  public getCourseRegistration = (): string => {
    return this.courseRegistration;
  };
  // private methods
  public launch = (): Promise<Result> => {
    let launchParams = parseLaunchParams(new URL(window.location.href));

    return new Promise(async (resolve, _) => {
      if (this.isTinCanLaunch(launchParams)) {
        if (launchParams.hasOwnProperty('registration')) {
          this._lessonRegistration = launchParams['registration'];
        }
        this._config = { ...launchParams, strictCallbacks: true };
        this._statementObject.actor = launchParams.actor;
        this._ADL.XAPIWrapper.changeConfig(this._config);
        return resolve({});
      } else {
        const results = await this.adlLaunch();
        return resolve(results as Result);
      }
    })
  };

  private isTinCanLaunch = (launchParams: { endpoint?: string, auth?: string, actor?: string }) => {
    return launchParams.hasOwnProperty('endpoint') &&
      launchParams.hasOwnProperty('auth') &&
      launchParams.hasOwnProperty('actor')
  }
  private adlLaunch = (): Promise<{} | { error: Error }> => {
    return new Promise((resolve, reject) => {
      this._ADL.launch((err: any, _: any, xAPIWrapper: any) => {
        if (!err) {
          this._ADL.XAPIWrapper = xAPIWrapper;
          return resolve({})
        } else {
          if (this._config === null) {
            reject(new Error(ERROR.LAUNCH_PARAMETERS_OR_CONFIGURATION_OBJECT_ERROR))
            return
          }
          this._ADL.XAPIWrapper.changeConfig(this._config);
          this._statementObject.actor = this._config.actor;
          return resolve({})
        }
      }, true);
    })
  }

  private createCourseAttemptState = (options: {} = {}): ICourseAttempt => {
    return {
      registration: '',
      initialized: false,
      terminated: false,
      lastSuspendedAttempt: '',
      bookmark: '',
      lessons: {},
      ...options,
    };
  };
  private createLessonAttemptState = (options: {} = {}): ILessonAttempt => {
    return {
      registration: '',
      initialized: false,
      terminated: false,
      suspended: false,
      bookmark: '',
      result: {},
      ...options,
    };
  };

  private addLessonAttempt = (state: ILessonAttempt, key: string, lessonAttempt) => {
    let clone: any = { ...state };

    if (clone.lessons && clone.lessons[key]) {
      clone?.lessons[key].push(lessonAttempt);
    } else {
      clone.lessons = {
        ...clone.lessons,
        [key]: [lessonAttempt],
      };
    }
    return clone;
  };

  private updateCourseActivityState = (state: ICourseAttempt, activityType: ACTIVITY, update: any = {}): ICourseAttempt => {
    let clone: any = { ...state };
    const { object: { id } } = this._statementObject as IStatementConfiguration;

    // push the update lesson attempt state back into the course state object
    if (activityType === ACTIVITY.LESSON) {
      if (Object.keys(clone?.lessons as Object).length === 0) return {} as ICourseAttempt;

      let foundLessonIndex = -1;
      if (clone.lessons) {
        foundLessonIndex = clone.lessons[id].findIndex(
          lesson => lesson.registration === this.lessonRegistration
        );
      }

      if (foundLessonIndex === -1) {
        throw Error(ERROR.LESSON_ATTEMPT_NOT_FOUND);
      }

      clone.lessons[id][foundLessonIndex] = {
        ...clone.lessons[id][foundLessonIndex],
        ...update,
      };
    } else {
      clone = { ...state, ...update };
    }

    return clone;
  };

  private updateLessonActivityState = (state: ILessonAttempt, update: any = {}): ILessonAttempt => {
    let clone: any = { ...state };

    if ('result' in update) {
      clone = {
        ...clone,
        result: {
          ...clone?.result,
          ...update?.result,
          score: {
            ...clone?.result?.score,
            ...update?.result?.score,
          },
        },
      };
    } else {
      clone = {
        ...clone,
        ...update,
      };
    }
    return clone;
  };

  /**
   *
   * @param activityID
   * @param registration
   * @returns
   */
  public getActivityState = (activityID: string): Promise<ICourseAttempt> => {
    const { actor } = this._config as IConfiguration;

    return new Promise((resolve, reject) => {
      this._ADL.XAPIWrapper.getState(
        activityID,
        actor,
        'https://w3id.org/xapi/scorm/activity-state',
        null,
        null,
        (error, { response, status }, _obj) => {
          if (status >= 200 && status < 300) {
            resolve(JSON.parse(response));
          } else {
            reject(error);
          }
        }
      );
    });
  };

  /**
   *
   * @param id
   * @param registration
   * @param attempt
   * @returns
   */

  private setActivityState = (id: string, state: ICourseAttempt | ILessonAttempt, registration: string | null): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (this._config) {
        this._ADL.XAPIWrapper.sendState(
          id,
          this._config.actor,
          'https://w3id.org/xapi/scorm/activity-state',
          registration,
          state,
          null, //  matchHash
          null, // noneMatchHash
          (error, { response, status }, _obj) => {
            if (status >= 200 && status < 300) {
              resolve({});
            } else {
              reject(error);
            }
          }
        );
      } else {
        reject('No configured actor set.');
      }
    });
  };

  /**
   * Sends statement
   * @param configuration
   * @param [attachments]
   * @returns statement
   */
  private sendStatement(statement: any, attachments?: Array<IAttachment>): Promise<Result> {
    // set generated internal statement for printing out
    this._statement = statement;

    return new Promise((resolve, reject) => {
      const cb = (error, { response, status }, obj) => {
        if (status >= 200 && status < 300) {
          resolve({});
        } else {
          reject(error);
        }
      };
      this._ADL.XAPIWrapper.sendStatement(statement, cb, attachments);
    });
  }

  private set lessonRegistration(registration: string) {
    this._lessonRegistration = registration;
  }
  private get lessonRegistration(): string {
    return this._lessonRegistration as string;
  }
  private set courseRegistration(registration: string) {
    this._courseRegistration = registration;
  }
  private get courseRegistration(): string {
    return this._courseRegistration as string;
  }

  // ACCESSORS
  get ACTIVITY() {
    return ACTIVITY;
  }
  get VERB() {
    return VERB;
  }
  get INTERACTION() {
    return INTERACTION;
  }

  get SCORE() {
    return SCORE;
  }

  get EXTENSIONS() {
    return EXTENSIONS;
  }

  get ACTIVITY_TYPES() {
    return ACTIVITY_TYPES;
  }

  // statement to string
  get statement() {
    return JSON.stringify(this._statement, undefined, 2);
  }

  get activity() {
    return this._statementObject;
  }
}