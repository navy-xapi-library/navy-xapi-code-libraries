/** ***********************************************************************
*
* Veracity Technology Consultants CONFIDENTIAL
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Veracity Technology Consultants and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Veracity Technology Consultants
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Veracity Technology Consultants.
*/

// utils
import { ruuid, getVerbID } from "../utils/utils";

// constants
import { ACTIVITY_TYPES } from "../constants/constants";

const addActor = (stmt, { actor }) => {
  return { ...stmt, actor };
};
const addVerb = (statement, { verb }) => {
  let stmt = { ...statement };
  stmt.verb = {
    id: getVerbID(verb),
    display: { en: verb },
  };
  return stmt;
};
const addObject = (stmt, { activityType, object, activity_extensions }) => {
  let s = {
    ...stmt,
    object: {
      ...object,
      definition: {
        ...object.definition,
        type: activityType,
        extensions: activity_extensions
      },
    },
  };
  s.object.definition.extensions
  return s;
};
const addContext = (stmt, { registration }) => {
  let context = {
    contextActivities: {},
    registration,
  };

  return { ...stmt, context: context };
};
const addParent = (stmt, { parent }) => {
  let p = [{...parent }];

  return {
    ...stmt,
    context: {
      ...stmt.context,
      contextActivities: { ...stmt.context.contextActivities, parent: p },
    },
  };
};
// make category configurable
const addCategory = (stmt, { isScorm, interaction }) => {
  let category = [];
  // default category
  category.push(
    {
      id: "https://w3id.org/xapi/netc/v1.0",
      definition: {
        type: "http://adlnet.gov/expapi/activities/profile",
      },
    }
  );
  category.push(
    {
      id: "https://w3id.org/xapi/netc-e-learning/v1.0",
      definition: {
        type: "http://adlnet.gov/expapi/activities/profile",
      },
    });

  if (isScorm) {
    category.push(
      {
        id: "https://w3id.org/xapi/scorm",
        definition: {
          type: "http://adlnet.gov/expapi/activities/profile",
        },
      });
  }
  if (interaction || stmt.object.type === ACTIVITY_TYPES.ASSESSMENT) {
    category.push(
      {
        id: "https://w3id.org/xapi/netc-assessment/v1.0",
        definition: {
          type: "http://adlnet.gov/expapi/activities/profile",
        },
      });
  } 
  return {
    ...stmt,
    context: { ...stmt.context, contextActivities: { category } },
  };
};

const addGrouping = (stmt, { group }) => {
  let grouping = [
    {
      ...group,
      definition: {
        ...group.definition,
        type: ACTIVITY_TYPES.COURSE,
      },
    },
  ];
  return {
    ...stmt,
    context: {
      ...stmt.context,
      contextActivities: { ...stmt.context.contextActivities, grouping },
    },
  };
};

const addInteraction = (statement, { object, interaction, activity_extensions }) => {
  const stmt = { ...statement };
  const { id, interactionType, definition } = interaction;

  stmt.object = {
    id,
    definition: {
      ...definition, // spread the rest of the possible options
      type: "http://adlnet.gov/expapi/activities/cmi.interaction",
      interactionType,
      extensions: activity_extensions
    },
  };
  // current activity is now part of parent since the interaction
  // is now the object
  if (stmt.context.contextActivities.parent) {
    stmt.context = {
      ...stmt.context,
      contextActivities: {
        ...stmt.context.contextActivities,
        parent: [
          ...stmt.context.contextActivities.parent,
          {
            ...object,
            definition: {
              ...object.definition,
              type: "http://adlnet.gov/expapi/activities/lesson",
            },
          },
        ],
      },
    };
  }

  return stmt;
};
const addGranular = (statement, { granular, object, activityType }) => {
  const stmt = { ...statement };
  const { id, definition } = object;

  stmt.object = {
    id,
    definition: {
      ...definition, // spread the rest of the possible options
      type: activityType,
    },
  };
  // current activity is now part of grouping since the interaction
  // is now the object
  if (stmt.context.contextActivities.grouping) {
    stmt.context = {
      ...stmt.context,
      contextActivities: {
        ...stmt.context.contextActivities,
        grouping: [          
          {
            ...granular,
            definition: {
              ...granular.definition,
              type: "http://adlnet.gov/expapi/activities/lesson",
            },
          },
          ...stmt.context.contextActivities.grouping
        ],
      },
    };
  }

  return stmt;
};

const addTimestamp = (stmt) => {
  return { ...stmt, timestamp: new Date().toISOString() };
};
const addResult = (stmt, { result }) => { 
  return { ...stmt, result };
};

const addExtensions = (stmt, { extensions }) => {
  return { ...stmt, context: { ...stmt.context, extensions } };
};
const addID = (stmt) => {
  return { ...stmt, id: ruuid() };
};
const addPlatform = (stmt, { platform }) => {
  return { ...stmt, context: { ...stmt.context, platform } };
};

export const generateStatement = (config) => {
  let statement = buildPipeline(config).reduce(
    (statement, func) => func(statement, config),
    {}
  );
  return statement;
};
// pipe of life
const buildPipeline = (config) => {
  const {
    group,
    parent = null,
    result = null,
    interaction = null,
    platform,
    granular,
  } = config;
  let pipeline = [
    addActor,
    addVerb,
    addObject,
    addContext,
    addCategory,
    addExtensions,
    addID,
    addTimestamp,
  ];

  if (group) {
    pipeline.push(addGrouping);
  }
  if (parent) {
    pipeline.push(addParent);
  }
  if (result) {
    pipeline.push(addResult);
  }
  if (interaction) {
    pipeline.push(addInteraction);
  }
  if (platform) {
    pipeline.push(addPlatform);
  }
  if (granular) {
    pipeline.push(addGranular);
  }

  return pipeline;
};
