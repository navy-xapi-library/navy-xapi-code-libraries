export const parseLaunchParams = (parsedURL, baseconf) => {

  const sp = parsedURL.search;
  const params = new URLSearchParams(sp);
  if(!baseconf){
    baseconf = {};
  }
  if (params.get("endpoint")) {
    baseconf["endpoint"] = params.get("endpoint");
  }
  if (params.get("auth")) {
    baseconf["auth"] = params.get("auth");
  }
  if (params.get("actor")) {
    baseconf["actor"] = params.get("actor");
  }
  if (params.get("registration")) {
    baseconf["registration"] = params.get("registration");
  }
  return baseconf;
};
export const ruuid = () => {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};
