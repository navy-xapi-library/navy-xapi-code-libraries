// custom
import _ADL from "../../xapiWrapper/ADL";

//utils
import { parseLaunchParams } from "./utils/utils";
import { generateStatement } from "./statements/statement-builder";

// constants
import { VERB, ACTIVITY, EXTENSIONS } from "./constants/constants";

// interfaces
import { Configuration, Result, Statement } from "./interfaces";

export class PerformanceSupportXAPI {
  private _config: Configuration | null = null;
  private _ADL: any;

  // statement object
  private _statementObject: any = {};
  private _statement: any;

  private _extensions = {};

  // used to store launch url parameters
  private _registration: string | null;
  private _activityID = null;
  private _grouping = null;

  private _platform = undefined;
  constructor(config) {
    if(config){
      this._config = {...config, strictCallbacks:true };
      this._platform = config.platform ? config.platform : null;
    }

    this._registration = null;
    this._ADL = _ADL();
    this.launch();
  }

  private sendStatement(stmt, attachments: Array<any> = []): Promise<Result> {
     // set generated internal statement for printing out
    this._statement = generateStatement(stmt);

    return new Promise((resolve, reject) => {
      const cb = ( error, response, obj ) => {
        if (response.status >= 200 && response.status < 300) {
          resolve({});
        } else {
          reject(error);
        }
      };
      this._ADL.XAPIWrapper.sendStatement(this._statement, cb, attachments);
    });
  }

  private launch = (): void => {
    let hasParams: boolean = false;
    //grab url parameters if any
    let launchParams = parseLaunchParams(
      new URL(window.location.href),
      this._config
    );
    // check to see if required url parameters were passed in
    if (launchParams.hasOwnProperty("endpoint") && launchParams.hasOwnProperty("auth") && launchParams.hasOwnProperty("actor")) {
      // set launch parameters values internally.
      if (launchParams.hasOwnProperty("registration")) {
        this._registration = launchParams["registration"];
      }
      // update xAPIWrapper configuration settings
      this._config = { ...launchParams, strictCallbacks:true };
      this._ADL.XAPIWrapper.changeConfig(this._config);
    } else {
      this._ADL.launch((err: any, launchdata: any, xAPIWrapper: any) => {
        if (!err) {
          // this was adl launched, use newly configured wrapper
          this._ADL.XAPIWrapper = xAPIWrapper;
        } else {
          if(this._config == null){
            throw new Error("No launch parameters or configuration object provided.");
          }
          this._ADL.XAPIWrapper.changeConfig(this._config);
        }
      }, true);
    }
  };
  private buildConfiguration(
    verb: string,
    activityType: string,
    options = null
  ): Statement {
    let stmt: Statement = {
      verb,
      activityType,
      actor: this._ADL.XAPIWrapper.lrs.actor,
      registration: this._registration,
      platform: this._platform ? this._platform : undefined,
      ...options as any,
    };

    return stmt;
  }
  private validateOptions = (options): Result => {
    // statementObj still empty?
    if (Object.entries(options).length === 0) {
      throw new Error("You must create a statement by using setActivity() and addGroup()");
    }
    if (!options.group) {
      throw new Error("A group is required to be able to send statements. Use addGroup() to add a group");
    }

    return {};
  };

  public setActivity(id: string, name: string, description: string): void {
    // id is required, but name and description are optional
    if (!id) {
      throw new Error('You must provided an "id" value.');
    }

    let stmt: any = { ...this._statementObject };
    // check to see if activityID was passed into the library as a launch parameter
    // else used the passed in argument
    stmt.activity = { id: this._activityID ? this._activityID : id };

    // if a name was provided add it to the activity object
    if (name) {
      stmt.activity.definition = {
        ...stmt.activity.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      stmt.activity.definition = {
        ...stmt.activity.definition,
        description: {
          en: description,
        },
      };
    }

    this._statementObject = stmt;
  }
  public setGroup(id: string, name: string, description: string): void {
    // id is required, but name and description are optional
    if (!id) {
      throw new Error('You must provided an "id" value.');
    }
    let stmt: any = { ...this._statementObject };
    stmt.group = { id };

    // if a name was provided add it to the activity object
    if (name) {
      stmt.group.definition = {
        ...stmt.group.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      stmt.group.definition = {
        ...stmt.group.definition,
        description: {
          en: description,
        },
      };
    }

    this._statementObject = stmt;
    // console.log("addGroup", this._statementObject);
  }

  public addExtensions(key: string | {}, value: string): void {
    // id is required
    if (!key) {
      throw new Error('You must provided an "key" value.');
    }
    // simple type check
    if (typeof key === "string") {
      if (!value) {
        throw new Error("Expecting 2 arguments, but only one was provided.");
      }
      this._extensions[key] = value;
    }
    // simple type check
    if (typeof key === "object") {
      // spread the old and new extensions
      this._extensions = { ...this._extensions, ...key };
    }
    // simple type check
    if (typeof key !== "string" && typeof key !== "object") {
      throw new Error("Parameter 1 needs to be either a string or an object.");
    }

    // spread them wings
    let stmt: any = {
      ...this._statementObject,
      extensions: this._extensions,
    };

    this._statementObject = stmt;
  }

  public createParent = (id: string, name: string, description: string) => {
    let stmt: any = {};

    if (!id) {
      throw Error('You must provided an "id" value.');
    }

    stmt = { id };

    // if a name was provided add it to the activity object
    if (name) {
      stmt.definition = {
        ...stmt.definition,
        name: {
          en: name,
        },
      };
    }
    // if a description was provided add it to the activity object
    if (description) {
      stmt.definition = {
        ...stmt.definition,
        description: {
          en: description,
        },
      };
    }

    return stmt;
  };
  public initialize(id : string, name : string, description : string): Promise<Result> {
    // set initialized activity
    this.setActivity(id, name, description);
    // set group to be used with other functions and statements
    this.setGroup(id, name, description);

    this._registration = this._registration
      ? this._registration
      : this._ADL.ruuid();

    let stmt = this.buildConfiguration(VERB.INITIALIZED, ACTIVITY.INITIALIZED, {
      ...this._statementObject,
    });

    return this.sendStatement(stmt);
  }
  public terminate(    
    id: string,
    name: string,
    description: string
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);    
    }

    let stmt = this.buildConfiguration(VERB.TERMINATED, ACTIVITY.TERMINATED, {
      ...this._statementObject,
      activity: this._statementObject.group,
      group: undefined,
    });
    return this.sendStatement(stmt);
  }
  public accessed(
    type : string,
    id: string,
    name: string,
    description: string,
    parent: string
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    // force upper case since constant declarations are always uppercase
    // if a dash is part of the constant value then replace it with a underscore to match
    // the constant declaration name
    let accessedType = type.toUpperCase().replace("-", "_");

    if(type !== ACTIVITY.MENU && type !== ACTIVITY.MENU_ITEM){
      throw new Error('accessed type must be either type "menu" or "menu-item"');
    }

    if (!parent && type === ACTIVITY.MENU_ITEM) {
      throw new Error("A parent object is required.");
    }

    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(VERB.ACCESSED, ACTIVITY[accessedType], {
      ...this._statementObject,
      parent,
    });
    return this.sendStatement(stmt);
  }
  //ACTIVITY.LINK
  public selected(
    type : string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error(isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    if(type !== ACTIVITY.CHECKLIST_ITEM && type !== ACTIVITY.LINK){
      throw new Error("Selected must be either type checklist item or link.")
    }
    if(!parent){
      throw new Error("A parent object is required.")
    }
    // force upper case since constant declarations are always uppercase
    // if a dash is part of the constant value then replace it with a underscore to match
    // the constant declaration name
    const selected = type.toUpperCase().replace("-", "_");

    // set activity
    this.setActivity(id, name, description);
    
    // build statement configuration
    let stmt = this.buildConfiguration(
    VERB.SELECTED,
      ACTIVITY[selected],
      {
        ...this._statementObject,
        parent,
      }
    );

    return this.sendStatement(stmt);
  }
  public deselected(
    type : string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error(isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }

    if(!parent){
      throw new Error("A parent object is required.")
    }
    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(
    VERB.DESELECTED,
      ACTIVITY.CHECKLIST_ITEM,
      {
        ...this._statementObject,
        parent,
      }
    );

    return this.sendStatement(stmt);
  }

  public completedChecklist(
    id: string,
    name: string,
    description: string
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }

    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(VERB.COMPLETED, ACTIVITY.CHECKLIST, {
      ...this._statementObject,
    });
    return this.sendStatement(stmt);
  }

  public liked(
    type: string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    // force upper case since constant declarations are always uppercase
    // if a dash is part of the constant value then replace it with a underscore to match
    // the constant declaration name
    const likedValue = type.toUpperCase().replace("-", "_");

    if (!ACTIVITY[likedValue]) {
      throw new Error(`${type} isn't a valid activity type.`);
    }

    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(VERB.LIKED, ACTIVITY[likedValue], {
      ...this._statementObject,
      parent,
    });

    return this.sendStatement(stmt);
  }

  public disliked(
    type: string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    // force upper case since constant declarations are always uppercase
    // if a dash is part of the constant value then replace it with a underscore to match
    // the constant declaration name
    const dislikedValue = type.toUpperCase().replace("-", "_");
    if (!ACTIVITY[dislikedValue]) {
      throw new Error(`${type} isn't a valid activity type.`);
    }

    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(VERB.DISLIKED, ACTIVITY[dislikedValue], {
      ...this._statementObject,
      parent,
    });
    return this.sendStatement(stmt);
  }

  public searched(
    value: string,
    id: string,
    name: string,
    description: string
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(VERB.SEARCHED, ACTIVITY.SEARCH_ENGINE, {
      ...this._statementObject,
      result: { response: value },
    });
    return this.sendStatement(stmt);
  }

  public viewed(
    value: string,
    id: string,
    name: string,
    description: string,
    parent: any
  ): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }

    let viewedValue = value.toLocaleLowerCase();
    if (
      viewedValue !== ACTIVITY.STEP &&
      viewedValue !== ACTIVITY.PROCEDURE &&
      viewedValue !== ACTIVITY.PAGE
    ) {
      throw new Error('You must pass in either "step", "procedure", or "page" as a value.');
    }
    if (value === ACTIVITY.STEP && !parent) {
      throw new Error("A parent object is required.");
    }

    // set activity
    this.setActivity(id, name, description);

    let stmt = this.buildConfiguration(VERB.VIEWED, viewedValue, {
      ...this._statementObject,
      parent,
    });

    return this.sendStatement(stmt);
  }
  public opened = (
    type: string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> => {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }

    if (!parent) {
      throw new Error("A parent object is required.");
    }

    let openedValue = type.toLocaleLowerCase();
    if (openedValue !== ACTIVITY.FILE && openedValue !== ACTIVITY.LINK) {
      throw new Error('You must pass in either "file" or "link" as a value.');
    }
    // set activity
    this.setActivity(id, name, description);

    let stmt = this.buildConfiguration(VERB.OPENED, openedValue, {
      ...this._statementObject,
      parent,
    });

    return this.sendStatement(stmt);
  };
  public closed = (
    type: string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> => {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    // force upper case since constant declarations are always uppercase
    // if a dash is part of the constant value then replace it with a underscore to match
    // the constant declaration name
    const closedValue = type.toUpperCase().replace("-", "_");

    if (!ACTIVITY[closedValue]) {
      throw new Error(`${type} isn't a valid activity type.`);
    }
    // set activity
    this.setActivity(id, name, description);

    let stmt = this.buildConfiguration(VERB.CLOSED, closedValue, {
      ...this._statementObject,
      parent,
    });

    return this.sendStatement(stmt);
  };
  public printed = (
    type: string,
    id: string,
    name: string,
    description: string,
    parent
  ): Promise<Result> => {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }
    // force upper case since constant declarations are always uppercase
    // if a dash is part of the constant value then replace it with a underscore to match
    // the constant declaration name
    const printedValue = type.toUpperCase().replace("-", "_");
    if (printedValue !== ACTIVITY.FILE && printedValue !== ACTIVITY.IMAGE && printedValue !== ACTIVITY.PAGE) {
      throw new Error('You must pass in either "file", "image" or "page" as a value.');
    }

    // set activity
    this.setActivity(id, name, description);

    let stmt = this.buildConfiguration(VERB.PRINTED, printedValue, {
      ...this._statementObject,
      parent,
    });

    return this.sendStatement(stmt);
  };
  public attachment = (value, usageType, contentType, display, description) => {
    if (!value) {
      throw new Error("You must provide a value.");
    }
    if (!usageType) {
      throw new Error("You must provide a usageType.");
    }
    if (!contentType) {
      throw new Error("You must provide a contentType.");
    }
    if (!display) {
      throw new Error("You must provide a contentType.");
    }

    // build attachment array
    return [
      {
        type: {
          usageType: this.getUsageType(usageType),
          display: { en: display },
          description: description ? { en: description } : undefined, // undefined won't set the property
          contentType,
        },
        value,
      },
    ];
  };
  public uploaded(id : string, name : string, description : string, attachment): Promise<Result> {
    // validate if a registration uuid has been assign yet
    let isReg = this.hasRegistration(this._registration as string);
    if (isReg.error) throw new Error( isReg.error );

    // is there an activity and group set yet?
    let validOptions = this.validateOptions(this._statementObject);
    if (validOptions.error) {
      throw new Error(validOptions.error);
    }

    // set activity
    this.setActivity(id, name, description);

    // build statement configuration
    let stmt = this.buildConfiguration(VERB.UPLOADED, ACTIVITY.FILE, {
      ...this._statementObject,
    });
    return this.sendStatement(stmt, attachment);
  }

  private hasRegistration = (registration: string): Result => {
    return registration
      ? {}
      : {
          error: `No registration id is associated with this attempt. You must call initialize to register a new attempt.`,
        };
  };
  private getUsageType = (type) => {
    let types = {};
    types["image"] = "http://adlnet.gov/expapi/attachments/image";
    types["signature"] = "http://adlnet.gov/expapi/attachments/signature";
    types["certification"] =
      "http://id.tincanapi.com/attachment/certificate-of-completion";

    return types[type];
  };

  public getAttemptRegistration = (): string => {
    return this._registration as string;
  };

  // ACTIVITY CONSTANTS
  get ACTIVITY() {
    return ACTIVITY;
  }

  // EXTENSIONS CONSTATNS
  get EXTENSIONS(){
    return EXTENSIONS;
  }
  // statement to string
  get statement() {
    return JSON.stringify(this._statement, undefined, 2);
  }
}
