// VERB TYPES
const ACCESSED = "accessed";
const COMPLETED = "completed";
const DESELECTED = "deselected";
const DISLIKED = "disliked";
const INITIALIZED = "initialized";
const LIKED = "liked";
const OPENED = "opened";
const SEARCHED = "searched";
const SELECTED = "selected";
const TERMINATED = "terminated";
const UPLOADED = "uploaded";
const VIEWED = "viewed";
const CLOSED = "closed";
const PRINTED = "printed";

// ACTIVITY TYPE
const APPLICATION = "application";
const CHECKLIST = "checklist";
const CHECKLIST_ITEM = "checklist-item";
const IMAGE = "image";
const MENU = "menu";
const MENU_ITEM = "menu-item";
const SEARCH_ENGINE = "search-engine";
const STEP = "step";
const PROCEDURE = "procedure";
const FILE = "file";
const LINK = "link";
const PAGE = "page";

// EXTENSIONS
const SCHOOL_CENTER =
  "https://w3id.org/xapi/netc/extensions/school-center";
const W3ID_EXTENSION = "https://w3id.org/xapi/performance-support/extensions";

// VERB BASE IRI
const VERB_TINCAN_IRI = "http://id.tincanapi.com/verb";
const VERB_ACROSSX_IRI = "https://w3id.org/xapi/acrossx/verbs";
const VERB_EXAPI_IRI = "http://adlnet.gov/expapi/verbs";
const VERB_W3ID = "https://w3id.org/xapi/performance-support/verbs";

// ACTIVITY BASE IRI
const ACTIVITY_W3ID_IRI =
  "https://w3id.org/xapi/performance-support/activity-types";
const ACTIVITY_TINCAN_IRI = "http://id.tincanapi.com/activitytype";
const ACTIVITY_ADL_IRI = "http://adlnet.gov/expapi/activities";
const ACTIVITY_ACROSSX_IRI = "https://w3id.org/xapi/acrossx/activities";
const ACTIVITY_W3ID_NETC = "https://w3id.org/xapi/netc/verbs/";

const PERFORMANCE_SUPPORT_IRI = "https://w3id.org/xapi/perfomance-support/v1.0";

const CATEGORY_TYPE_IRI = "http://adlnet.gov/expapi/activities/profile";
const CATEGORY_NETC_IRI = "https://w3id.org/xapi/netc/v1.0";



// grouping context = application / simulation / course
export const getParentType = (type) => {
  switch (type) {
    case LINK:
    case SEARCH_ENGINE:
      return `${ACTIVITY_ACROSSX_IRI}/${SEARCH_ENGINE}`;
    case FILE:
    case IMAGE:
      return `${ACTIVITY_ACROSSX_IRI}/${PAGE}`;
    case CHECKLIST_ITEM:
    case CHECKLIST:
      return `${ACTIVITY_TINCAN_IRI}/${CHECKLIST}`;
    case STEP:
      return `${ACTIVITY_W3ID_IRI}/${type}`;
    case MENU_ITEM:
      return `${ACTIVITY_W3ID_IRI}/${MENU}`;
    default:
      return undefined;
  }
};

export const getVerbBaseURL = (verb) => {
  switch (verb) {
    case INITIALIZED:
    case TERMINATED:
    case COMPLETED:
      return VERB_EXAPI_IRI;
    case OPENED:
    case SEARCHED:
    case DESELECTED:
    case ACCESSED:
      return VERB_W3ID;
    case SELECTED:
    case VIEWED:
      return VERB_TINCAN_IRI;
    case UPLOADED:
      return ACTIVITY_W3ID_NETC;
    case LIKED:
    case DISLIKED:
      return VERB_ACROSSX_IRI;
    default:
      throw new Error(`No such verb type ${verb}`);
  }
};

export const getActivityTypeID = (type) => {
  switch (type) {
    case INITIALIZED:
    case TERMINATED:
      return `${ACTIVITY_W3ID_IRI}/${APPLICATION}`;
    case LINK:
      return `${ACTIVITY_ADL_IRI}/${type}`;
    case CHECKLIST:
      return `${ACTIVITY_TINCAN_IRI}/${type}`;
    case CHECKLIST_ITEM:
      return `${ACTIVITY_TINCAN_IRI}/${CHECKLIST_ITEM}`;
    case FILE:
      return `${ACTIVITY_ADL_IRI}/${FILE}`;
    case MENU:
      return `${PERFORMANCE_SUPPORT_IRI}/${MENU}`;
    case APPLICATION:
    case IMAGE:
    case MENU_ITEM:
    case PROCEDURE:
      return `${ACTIVITY_W3ID_IRI}/${type}`;
    case PAGE:
    case SEARCH_ENGINE:
      return `${ACTIVITY_ACROSSX_IRI}/${type}`;
    case STEP:
      return `${ACTIVITY_TINCAN_IRI}/${type}`;
    default:
      return `${ACTIVITY_W3ID_IRI}/${APPLICATION}`;
  }
};
export const getContextTypeID = (verb) => {
  return `${ACTIVITY_W3ID_IRI}/${APPLICATION}`;
};

export const VERB = {
  COMPLETED,
  SEARCHED, 
  SELECTED, 
  VIEWED, 
  UPLOADED, 
  LIKED, 
  DISLIKED, 
  INITIALIZED,
  TERMINATED,
  DESELECTED,
  ACCESSED,
  OPENED,
  CLOSED,
  PRINTED
};

export const ACTIVITY = {
  INITIALIZED,
  TERMINATED,
  CHECKLIST,
  CHECKLIST_ITEM,
  FILE,
  IMAGE,
  MENU,
  APPLICATION,
  SEARCH_ENGINE,
  LINK,
  STEP,
  PROCEDURE,
  MENU_ITEM,
  PAGE
};

export const EXTENSIONS = {
  SCHOOL_CENTER
};
export const BASE_IRI = {
  VERB_TINCAN_IRI,
  VERB_ACROSSX_IRI,
  VERB_EXAPI_IRI,
  ACTIVITY_W3ID_IRI,
  ACTIVITY_TINCAN_IRI,
  ACTIVITY_ADL_IRI,
  W3ID_EXTENSION,
  PERFORMANCE_SUPPORT_IRI,
  CATEGORY_TYPE_IRI,
  CATEGORY_NETC_IRI,
};