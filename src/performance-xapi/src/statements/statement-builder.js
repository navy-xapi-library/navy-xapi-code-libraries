/** ***********************************************************************
*
* Veracity Technology Consultants CONFIDENTIAL
* __________________
*
*  2019 Veracity Technology Consultants
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Veracity Technology Consultants and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Veracity Technology Consultants
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Veracity Technology Consultants.
*/


import {
  VERB,
  BASE_IRI,
  getVerbBaseURL,
  getActivityTypeID,
  getContextTypeID,
  getParentType,
} from "../constants/constants";

import { ruuid } from "../utils/utils";

const addActor = (stmt, { actor }) => {
  return { ...stmt, actor };
};

const addVerb = (stmt, { verb }) => {
  return {
    ...stmt,
    verb: {
      id: `${getVerbBaseURL(verb)}/${verb}`,
      display: { "en": verb },
    },
  };
};

const addObject = (stmt, { activityType, activity }) => {
  return {
    ...stmt,
    object: {
      ...activity,
      definition: {
        ...activity.definition,
        type: getActivityTypeID(activityType),
      },
    },
  };
};

const addContext = (stmt, { registration }) => {
  let context = {
    contextActivities: {},
    registration,
  };

  return { ...stmt, context: context };
};
const addParent = (stmt, { activityType, parent }) => {
  let p = [
    {
      ...parent,
      definition: { ...parent.definition, type: getParentType(activityType) },
    },
  ];

  return {
    ...stmt,
    context: {
      ...stmt.context,
      contextActivities: { ...stmt.context.contextActivities, parent: p },
    },
  };
};
const addCategory = (stmt) => {
  let category = [
    {
      id: BASE_IRI.PERFORMANCE_SUPPORT_IRI,
      definition: {
        type: BASE_IRI.CATEGORY_TYPE_IRI,
      },
    },
    {
      id: BASE_IRI.CATEGORY_NETC_IRI,
      definition: {
        type: BASE_IRI.CATEGORY_TYPE_IRI,
      },
    },
  ];
  return {
    ...stmt,
    context: { ...stmt.context, contextActivities: { category } },
  };
};
const addGrouping = (stmt, { activityType, group }) => {
  let grouping = [
    {
      ...group,
      definition: {
        ...group.definition,
        type: getContextTypeID(activityType),
      },
    },
  ];
  return {
    ...stmt,
    context: {
      ...stmt.context,
      contextActivities: { ...stmt.context.contextActivities, grouping },
    },
  };
};
const addTimestamp = (stmt) => {
  return { ...stmt, timestamp: new Date().toISOString() };
};
const addResult = (stmt, { result }) => {
  return { ...stmt, result };
};

const addExtensions = (stmt, { extensions }) => {
  return { ...stmt, context: { ...stmt.context, extensions } };
};
const addID = (stmt) => {
  return { ...stmt, id: ruuid() };
};
const addPlatform = (stmt, { platform }) => {
  return { ...stmt, context: { ...stmt.context, platform } };
};
// print the current statement state
const addConsole = (stmt) => {
  const { verb, object } = stmt;
  const { display } = verb;
  const { type } = object.definition;
  console.log(`************* ${display["en-US"]} *************`);
  console.log(JSON.stringify(stmt, undefined, 2));
  return stmt;
};

export const generateStatement = (config) => {
  let statement = buildPipeline(config).reduce(
    (statement, func) => func(statement, config),
    {}
  );
  return statement;
};

const buildPipeline = (config) => {
  const { verb, parent, result, platform } = config;
  let pipeline = [
    addActor,
    addVerb,
    addObject,
    addContext,
    addCategory,
    addExtensions,
    addID,
    addTimestamp,
  ];
  switch (verb) {
    case VERB.INITIALIZED:
    case VERB.TERMINATED:
      break;
    case VERB.SELECTED:
    case VERB.DESELECTED:
    case VERB.VIEWED:
    case VERB.COMPLETED:
    case VERB.LIKED:
    case VERB.DISLIKED:
    case VERB.UPLOADED:
    case VERB.SEARCHED:
    case VERB.ACCESSED:
    case VERB.OPENED:
      pipeline.push(addGrouping);
      break;
    default:
      throw new Error(`No such statement type ${verb}`);
  }
  if (parent) {
    pipeline.push(addParent);
  }
  if (result) {
    pipeline.push(addResult);
  }
  if (platform) {
    pipeline.push(addPlatform);
  }
  return pipeline;
};
