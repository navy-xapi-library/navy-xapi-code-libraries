import { ELearningXAPI } from './e-learning-xapi/src/index';
import { PerformanceSupportXAPI } from './performance-xapi/src/index';

export { ELearningXAPI, PerformanceSupportXAPI };
