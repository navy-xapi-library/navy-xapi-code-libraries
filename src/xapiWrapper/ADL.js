import types from "./activitytypes";
import verbs from "./verbs";
import launch from "./xapi-launch";
import util from "./xapi-util";
import statement from "./xapistatement";
import wrapper from "./xapiwrapper";

let ADL;
export default function _ADL() {
    if(ADL) return ADL;
    ADL = {};
    types(ADL);
    verbs(ADL);
    launch(ADL);
    util(ADL);
    statement(ADL);
    wrapper(ADL);
    return ADL;
}