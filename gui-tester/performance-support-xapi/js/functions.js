const extensions = {
  "https://w3id.org/xapi/netc/extensions/school-center":
    "Center for Naval Aviation Technical Training (CNATT)",
};

var config = {
  endpoint: "https://performance-support-example.lrs.io/xapi/",
  auth: "Basic " + base64.encode("nofoop:jewrudsjj^#%GS3"),
  actor: {
    name: "Test Testerson",
    objectType: "Agent",
    account: {
      homePage: "https://edipi.navy.mil",
      name: "1625378541",
    },
  },
};

const form = document.getElementById("form");
const myFiles = document.getElementById("myFile");
const dropdown = document.getElementById("dropdownMenuButton");
let likedValue = null;
let dislikedValue = null;

form.addEventListener("submit", async (e) => {
  e.preventDefault();
  const attachmentBtn = document.getElementById("attachment-button");
  $(attachmentBtn).prop("disabled", true);
  // add spinner to button

  var reader = new FileReader();
  reader.onload = async function () {
    var arrayBuffer = this.result;
    console.log("attachment", arrayBuffer);
    // send file attachment to the lrs
    getResult("attachment", arrayBuffer);
  };
  reader.readAsArrayBuffer(myFiles.files[0]);
});

function addSpinner(element) {
  $(document).ready(function () {
    $(element).click(function () {
      // disable button
      $(this).prop("disabled", true);
      // add spinner to button
      $(this).html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
      );
    });
  });
}
function removeSpinner(element, text) {
  element.innerHTML = text;
  element.disabled = false;
  $("span").remove(".spinner-border");
}
function hideShowSuccess(element, toggle) {
  $(`${element}`).css("display", toggle ? "block" : "none");
}

async function start() {
  [
    "#launch-button",
    "#vieweds-button",
    "#viewedp-button",
    "#viewedpage-button",
    "#menu-button",
    "#menui-button",
    "#checklist-button",
    "#complete-button",
    "#liked-button",
    "#disliked-button",
    "#search-button",
    "#select-button",
    "#opened-button",
    // "#attachment-button",
    "#exit-button",
  ].forEach((elem) => {
    addSpinner(elem);
    hideShowSuccess(elem.substring(0, elem.indexOf("-")), false);
  });

  $(".liked li a").click(function () {
    var selText = $(this).text();
    $(this)
      .parents(".btn-group")
      .find(".dropdown-toggle")
      .html(selText + ' <span class="caret"></span>');
    likedValue = selText;
    $("#liked-button").removeClass("disabled");
    $("#liked-button").css("pointer-events", "auto");
  });
  $(".disliked li a").click(function () {
    var selText = $(this).text();
    $(this)
      .parents(".btn-group")
      .find(".dropdown-toggle")
      .html(selText + ' <span class="caret"></span>');
    dislikedValue = selText;
    $("#disliked-button").removeClass("disabled");
    $("#disliked-button").css("pointer-events", "auto");
  });

  // special case
  hideShowSuccess("#attachment");
}

const { PerformanceSupportXAPI } = veracity;
// lib
var performanceSupport = new PerformanceSupportXAPI(config);
//  add extensions
performanceSupport.addExtensions(extensions);

const { ACTIVITY } = performanceSupport;


async function getResult(id, payload) {
  let result = null;
  let response = null;
  let parent;

  switch (id) {
    case "initialize":
      // result = document.getElementById("launch-result");
      response = await performanceSupport
        .initialize(
          "https://navy.mil/netc/xapi/activities/applications/1b8d161c-c14f-11ea-b3de-0242ac130004",
          "Performance Support App",
          "A mobile application used at the moment of need for performance support."
        )
        .catch((error) => console.log(error));
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("launch-button"), "Launch");

      hideShowSuccess("#launch", true);
      break;
    case "viewed-step":
      // result = document.getElementById("launch-result");
      parent = performanceSupport.createParent(
        "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
        "Auxiliary Equipment Maintenance Procedure",
        "A maintenance procedure required to ensure proper auxiliary equipment operation."
      );

      response = await performanceSupport.viewed(
        "step",
        "https://navy.mil/netc/xapi/activities/steps/382927d2-b0e9-11ea-b3de-0242ac130004",
        "Step 1",
        "Clean the auxiliary equipment",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("vieweds-button"), "Launch");

      hideShowSuccess("#vieweds", true);
      break;
    case "viewed-procedure":
      // result = document.getElementById("launch-result");
      response = await performanceSupport.viewed(
        "procedure",
        "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
        "Auxiliary Equipment Maintenance Procedure",
        "A maintenance procedure required to ensure proper auxiliary equipment operation."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("viewedp-button"), "Launch");

      hideShowSuccess("#viewedp", true);
      break;
    case "viewed-page":
      // result = document.getElementById("launch-result");
      response = await performanceSupport.viewed(
        "page",
        "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
        "Auxiliary Equipment Maintenance Procedure",
        "A maintenance procedure required to ensure proper auxiliary equipment operation."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("viewedpage-button"), "Launch");

      hideShowSuccess("#viewedpage", true);
      break;
    case "menu-select":
      response = await performanceSupport.accessed(
        ACTIVITY.MENU,
        "https://navy.mil/netc/xapi/activities/menus/a0db418e-961d-416d-9051-49ac3f812bc2",
        "AAS App Menu",
        "The main navigation menu provided for the Advanced Airborne Sensor App."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("menu-button"), "Submit");
      hideShowSuccess("#menu", true);
      break;
    case "menu-item-select":
      // result = document.getElementById("menu-result");
      parent = performanceSupport.createParent(
        "https://navy.mil/netc/xapi/activities/menus/25fcb754-b09e-11ea-b3de-0242ac130004",
        "Configuration Menu",
        "The configuration menu provided for the Advanced Airborne Sensor App."
      );
      response = await performanceSupport.accessed(
        ACTIVITY.MENU_ITEM,
        "https://navy.mil/netc/xapi/activities/menu-items/6ac32904-0774-49d8-9527-ccfb264cce03",
        "AAS Configuration",
        "A submenu item in the Configuration Menu.",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("menu-item-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#menui", true);
      }
      break;

    case "checklist-item":
      // result = document.getElementById("checklist-result");
      let checked = document.getElementById("exampleCheck1").checked;

      parent = performanceSupport.createParent(
        "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
        "Advanced Airborne Sensor Maintenance Checklist",
        "A maintenance checklist for the Advanced Airborne Sensor on the P-8"
      );

      response = checked
        ? await performanceSupport.selected(
            ACTIVITY.CHECKLIST_ITEM,
            "https://navy.mil/netc/xapi/activities/checklist-items/f8be33ad-cb1b-480c-ae91-4444114ba065",
            "Cleaned and Inspected Fiber Cables and Umbilicals",
            "Checklist item representing the procedure to perform cleaning and inspection of fiber cables and umbilicals",
            parent
          )
        : await performanceSupport
            .deselected(
              ACTIVITY.CHECKLIST_ITEM,
              "https://navy.mil/netc/xapi/activities/checklist-items/f8be33ad-cb1b-480c-ae91-4444114ba065",
              "Cleaned and Inspected Fiber Cables and Umbilicals",
              "Checklist item representing the procedure to perform cleaning and inspection of fiber cables and umbilicals",
              parent
            )
            .catch((error) => console.log(performanceSupport.statement));
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("checklist-button"), "Submit");
      hideShowSuccess("#checklist", true);
      break;
    case "complete-checklist":
      const completeBtn = document.getElementById("complete-button");
      const form = Array.from(
        document.getElementById("checkbox-form").elements
      );
      const complete = form.filter((cb) => cb.checked);
      // result = document.getElementById("complete-result");

      if (complete.length === form.length) {
        response = await performanceSupport.completedChecklist(
          "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
          "Advanced Airborne Sensor Maintenance Checklist",
          "A maintenance checklist for the Advanced Airborne Sensor on the P-8."
        );
        // result.innerHTML = JSON.stringify(response, undefined, 2);
        hideShowSuccess("#complete", true);
        removeSpinner(completeBtn, "Submit");
      } else {
        alert("Complete the list already!");
        window.setTimeout(() => removeSpinner(completeBtn, "Submit"), 500);
      }
      break;
    case "liked":
      if (likedValue) {
        // parent = performanceSupport.createParent(
        //   `https://navy.mil/netc/xapi/activities/${likedValue}/2250e9ef-f105-4092-a53c-93cb25b8e088`,
        //   `SMP Inspection ${likedValue}`,
        //   `A ${likedValue} of SMP Avionics and Power Umbilical Inspection Setup.`
        // );
        response = await performanceSupport.liked(
          likedValue,
          `https://navy.mil/netc/xapi/activities/${likedValue}/326b1af6-b0a0-11ea-b3de-0242ac130004`,
          `Liked ${likedValue}`,
          `Liking a ${likedValue} Example`
          // parent
        );
        // result.innerHTML = JSON.stringify(response, undefined, 2);
        removeSpinner(document.getElementById("liked-button"), "Like");
        hideShowSuccess("#liked", true);
      }
      removeSpinner(document.getElementById("liked-button"), "Like");
      break;
    case "disliked":
      // result = document.getElementById("disliked-result");
      parent = performanceSupport.createParent(
        `https://navy.mil/netc/xapi/activities/${likedValue}/2250e9ef-f105-4092-a53c-93cb25b8e088`,
        `SMP Inspection ${likedValue}`,
        `A ${likedValue} of SMP Avionics and Power Umbilical Inspection Setup.`
      );
      response = await performanceSupport.disliked(
        dislikedValue,
        "http://navy.mil/netc/examples/performance-support/dislike",
        `Disliked ${dislikedValue}`,
        `Disliked ${dislikedValue} Example`,
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("disliked-button"), "Dislike");
      hideShowSuccess("#disliked", true);
      break;
    case "search":
      // result = document.getElementById("search-result");

      // fetch("https://jsonplaceholder.typicode.com/users/1/posts")
      //   .then((response) => response.json())
      //   .then((json) => console.log(json));

      response = await performanceSupport.searched(
        "SMPDM",
        "https://navy.mil/netc/xapi/activities/search-engine/489e9c1b-13ef-43ae-a7ab-ffe138c19f77",
        "Application Search",
        "A search activity performed using the application’s user interface."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("search-button"), "Submit");
      hideShowSuccess("#search", true);
      break;
    case "select-search-link":
      // result = document.getElementById("select-search-result");
      parent = performanceSupport.createParent(
        "https://navy.mil/netc/xapi/activities/search-engine/489e9c1b-13ef-43ae-a7ab-ffe138c19f77",
        "Application Search Engine",
        "The search engine used in the performance support application."
      );
      response = await performanceSupport
        .selected(
          ACTIVITY.LINK,
          "https://navy.mil/netc/xapi/activities/links/e4042887-6035-41bb-ae98-96d9f6e11884",
          "SMPDM Link",
          "A link to a resource on SMPDM.",
          parent
        )
        .catch((error) => console.log(error));
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("select-button"), "Submit");
      hideShowSuccess("#select", true);

      break;
    case "opened":
      parent = performanceSupport.createParent(
        "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
        "AAR Resources Page",
        "A resource page within the Advanced Airborne Sensor Performance Support App."
      );
      response = await performanceSupport.opened(
        ACTIVITY.FILE,
        "https://navy.mil/netc/xapi/activities/files/307325ca-36f4-49cb-8787-80faf527f042",
        "Advanced Airborne Sensor Special Mission Pod Deployment Mechanism Technical Manual, Aircraft Maintenance Manual (AAM) CDRL C027: 20 October 2015, D809-04231-1",
        "A PDF file tech manual for the Advanced Airborne Sensor.",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("opened-button"), "Submit");
      hideShowSuccess("#opened", true);
      break;
    case "attachment":
      let attachment = performanceSupport.attachment(
        payload,
        "image",
        "image/jpeg",
        "Image Attachment.",
        "Uploading An Image Attachment."
      );
      response = await performanceSupport.uploaded(
        "https://navy.mil/netc/xapi/activities/files/c132acb1-c4e1-4b7c-9798-badc4a587561",
        "Airborne Sensor Notes",
        "A text file of personal notes taken while training on the Airborne Sensor Lesson.",
        attachment
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("attachment-button"), "Submit");
      hideShowSuccess("#attachment", true);
      break;
    case "terminate":
      result = document.getElementById("exit-result");
      response = await performanceSupport.terminate(
        "https://navy.mil/netc/xapi/activities/applications/60608f6b-c938-4ae7-b170-816fdf5947da",
        "Advanced Airborne Sensor Performance Support App",
        "A mobile application used at the moment of need for performance support of the Advanced Airborne Sensor on the P-8."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("exit-button"), "Exit");
      hideShowSuccess("#exit", true);
      break;
    default:
      console.error(`No such type. ${id}`);
  }
  console.log(performanceSupport.statement);
}
start();
