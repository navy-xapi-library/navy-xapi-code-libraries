const extensions = {
  "https://w3id.org/xapi/netc/extensions/school-center":
    "Center for Naval Aviation Technical Training (CNATT)",
};

var config = {
  endpoint: "https://performance-support-example.lrs.io/xapi/",
  auth: "Basic " + base64.encode("nofoop:jewrudsjj^#%GS3"),
  actor: {
    name: "Test Testerson",
    objectType: "Agent",
    account: {
      homePage: "https://edipi.navy.mil",
      name: "1625378541",
    },
  },
};

const form = document.getElementById("form");
const myFiles = document.getElementById("myFile");
const dropdown = document.getElementById("dropdownMenuButton");

form.addEventListener("submit", async (e) => {
  e.preventDefault();
  const attachmentBtn = document.getElementById("attachment-button");
  $(attachmentBtn).prop("disabled", true);
  // add spinner to button

  var reader = new FileReader();
  reader.onload = async function () {
    var arrayBuffer = this.result;
    console.log("attachment", arrayBuffer);
    // send file attachment to the lrs
    getResult("attachment", arrayBuffer);
  };
  reader.readAsArrayBuffer(myFiles.files[0]);
});

function addSpinner(element) {
  $(document).ready(function () {
    $(element).click(function () {
      // disable button
      $(this).prop("disabled", true);
      // add spinner to button
      $(this).html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
      );
    });
  });
}
function removeSpinner(element, text) {
  element.innerHTML = text;
  element.disabled = false;
  $("span").remove(".spinner-border");
}
function hideShowSuccess(element, toggle) {
  $(`${element}`).css("display", toggle ? "block" : "none");
}

async function start() {
  [
    "#launch-button",
    "#vieweds-button",
    "#viewedp-button",
    "#menu-button",
    "#checklist-button",
    "#complete-button",
    "#liked-button",
    "#disliked-button",
    "#search-button",
    "#select-button",
    "#opened-button",
    // "#attachment-button",
    "#exit-button",
  ].forEach((elem) => {
    addSpinner(elem);
    hideShowSuccess(elem.substring(0, elem.indexOf("-")), false);
  });

  // special case
  hideShowSuccess("#attachment");
}

// lib
var xapi = new PerformanceSupportXAPI(config);

xapi.setActivity(
  "https://navy.mil/netc/xapi/activities/applications/60608f6b-c938-4ae7-b170-816fdf5947da",
  "Advanced Airborne Sensor Performance Support App",
  "A mobile application used at the moment of need for performance support of the Advanced Airborne Sensor on the P-8."
);
xapi.setGroup(
  "https://navy.mil/netc/xapi/activities/applications/60608f6b-c938-4ae7-b170-816fdf5947da",
  "Advanced Airborne Sensor Performance Support App",
  "A mobile application used at the moment of need for performance support of the Advanced Airborne Sensor on the P-8"
);
xapi.addExtensions(extensions);

async function getResult(id, payload) {
  let result = null;
  let response = null;
  let parent;

  switch (id) {
    case "initialize":
      // result = document.getElementById("launch-result");
      response = await xapi.initialize();
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("launch-button"), "Launch");

      if (!response.error) {
        hideShowSuccess("#launch", true);
      }
      break;
    case "viewed-step":
      // result = document.getElementById("launch-result");
      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
        "Auxiliary Equipment Maintenance Procedure",
        "A maintenance procedure required to ensure proper auxiliary equipment operation."
      );

      response = await xapi.viewed(
        "step",
        "https://navy.mil/netc/xapi/activities/steps/382927d2-b0e9-11ea-b3de-0242ac130004",
        "Step 1",
        "Clean the auxiliary equipment",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("vieweds-button"), "Launch");

      if (!response.error) {
        hideShowSuccess("#vieweds", true);
      }
      break;
    case "viewed-procedure":
      // result = document.getElementById("launch-result");
      response = await xapi.viewed(
        "procedure",
        "https://navy.mil/netc/xapi/activities/procedures/97668fef-b6bb-419a-8795-ede691dba9a8",
        "Auxiliary Equipment Maintenance Procedure",
        "A maintenance procedure required to ensure proper auxiliary equipment operation."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("viewedp-button"), "Launch");

      if (!response.error) {
        hideShowSuccess("#viewedp", true);
      }
      break;
    case "menu-select":
      // result = document.getElementById("menu-result");
      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/menus/25fcb754-b09e-11ea-b3de-0242ac130004",
        "Configuration Menu",
        "The configuration menu provided for the Advanced Airborne Sensor App."
      );
      response = await xapi.accessedMenuItem(
        "https://navy.mil/netc/xapi/activities/menu-items/6ac32904-0774-49d8-9527-ccfb264cce03",
        "AAS Configuration",
        "A submenu item in the Configuration Menu.",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("menu-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#menu", true);
      }
      break;
    case "checklist-item":
      // result = document.getElementById("checklist-result");
      let checked = document.getElementById("exampleCheck1").checked;

      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
        "Advanced Airborne Sensor Maintenance Checklist",
        "A maintenance checklist for the Advanced Airborne Sensor on the P-8"
      );

      response = await xapi.selectedChecklistItem(
        checked ? true : false,
        "https://navy.mil/netc/xapi/activities/checklist-items/f8be33ad-cb1b-480c-ae91-4444114ba065",
        "Cleaned and Inspected Fiber Cables and Umbilicals",
        "Checklist item representing the procedure to perform cleaning and inspection of fiber cables and umbilicals",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("checklist-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#checklist", true);
      }
      break;
    case "complete-checklist":
      const completeBtn = document.getElementById("complete-button");
      const form = Array.from(
        document.getElementById("checkbox-form").elements
      );
      const complete = form.filter((cb) => cb.checked);
      // result = document.getElementById("complete-result");

      if (complete.length === form.length) {
        response = await xapi.completedChecklist(
          "https://navy.mil/netc/xapi/activities/checklists/43b9c22c-6fae-4989-82ce-e02019744b38",
          "Advanced Airborne Sensor Maintenance Checklist",
          "A maintenance checklist for the Advanced Airborne Sensor on the P-8."
        );
        // result.innerHTML = JSON.stringify(response, undefined, 2);
        if (!response.error) {
          hideShowSuccess("#complete", true);
          removeSpinner(completeBtn, "Submit");
        }
      } else {
        alert("Complete the list already!");
        window.setTimeout(() => removeSpinner(completeBtn, "Submit"), 500);
      }
      break;
    case "liked":
      // result = document.getElementById("like-result");
      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
        "SMP Inspection Diagram",
        "A diagram of SMP Avionics and Power Umbilical Inspection Setup."
      );
      response = await xapi.liked(
        "https://navy.mil/netc/xapi/activities/images/326b1af6-b0a0-11ea-b3de-0242ac130004",
        "Liked Image",
        "Liking a Image Example",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("liked-button"), "Like");
      if (!response.error) {
        hideShowSuccess("#liked", true);
      }
      break;
    case "disliked":
      // result = document.getElementById("disliked-result");
      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
        "SMP Inspection Diagram",
        "A diagram of SMP Avionics and Power Umbilical Inspection Setup."
      );
      response = await xapi.disliked(
        "http://navy.mil/netc/examples/performance-support/dislike",
        "Disliked Image",
        "Disliking an Image Example",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("disliked-button"), "Dislike");
      if (!response.error) {
        hideShowSuccess("#disliked", true);
      }
      break;
    case "search":
      // result = document.getElementById("search-result");

      // fetch("https://jsonplaceholder.typicode.com/users/1/posts")
      //   .then((response) => response.json())
      //   .then((json) => console.log(json));

      response = await xapi.searched(
        "SMPDM",
        "https://navy.mil/netc/xapi/activities/search-engine/489e9c1b-13ef-43ae-a7ab-ffe138c19f77",
        "Application Search",
        "A search activity performed using the application’s user interface."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("search-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#search", true);
      }
      break;
    case "select-search-link":
      // result = document.getElementById("select-search-result");
      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/search-engine/489e9c1b-13ef-43ae-a7ab-ffe138c19f77",
        "Application Search Engine",
        "The search engine used in the performance support application."
      );
      response = await xapi.searchResult(
        "http://somelink2",
        "https://navy.mil/netc/xapi/activities/links/e4042887-6035-41bb-ae98-96d9f6e11884",
        "SMPDM Link",
        "A link to a resource on SMPDM.",
        ["http://somelink1", "http://somelink2", "http://somelink3"],
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("select-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#select", true);
      }
      break;
    case "opened":
      parent = xapi.createParent(
        "https://navy.mil/netc/xapi/activities/pages/2250e9ef-f105-4092-a53c-93cb25b8e088",
        "AAR Resources Page",
        "A resource page within the Advanced Airborne Sensor Performance Support App."
      );
      response = await xapi.opened(
        "https://navy.mil/netc/xapi/activities/files/307325ca-36f4-49cb-8787-80faf527f042",
        "Advanced Airborne Sensor Special Mission Pod Deployment Mechanism Technical Manual, Aircraft Maintenance Manual (AAM) CDRL C027: 20 October 2015, D809-04231-1",
        "A PDF file tech manual for the Advanced Airborne Sensor.",
        parent
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("opened-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#opened", true);
      }
      break;
    case "attachment":
      let attachment = xapi.attachment(
        payload,
        "image",
        "image/jpeg",
        "Image Attachment.",
        "Uploading An Image Attachment."
      );
      response = await xapi.uploaded(
        "https://navy.mil/netc/xapi/activities/files/c132acb1-c4e1-4b7c-9798-badc4a587561",
        "Airborne Sensor Notes",
        "A text file of personal notes taken while training on the Airborne Sensor Lesson.",
        attachment
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("attachment-button"), "Submit");
      if (!response.error) {
        hideShowSuccess("#attachment", true);
      }
      break;
    case "terminate":
      result = document.getElementById("exit-result");
      response = await xapi.terminate(
        "https://navy.mil/netc/xapi/activities/applications/60608f6b-c938-4ae7-b170-816fdf5947da",
        "Advanced Airborne Sensor Performance Support App",
        "A mobile application used at the moment of need for performance support of the Advanced Airborne Sensor on the P-8."
      );
      // result.innerHTML = JSON.stringify(response, undefined, 2);
      removeSpinner(document.getElementById("exit-button"), "Exit");
      if (!response.error) {
        hideShowSuccess("#exit", true);
      }
      break;
    default:
      console.error(`No such type. ${id}`);
  }
}
start();
