const { ELearningXAPI, PerformanceSupportXAPI } = navy;

const config = {
  endpoint: "https://elearningexample.lrs.io/xapi/",
  auth: "Basic " + base64.encode("gicukj:reoriu"),
  actor: {
    name: "Test Testerson",
    objectType: "Agent",
    account: {
      homePage: "https://edipi.navy.mil",
      name: "0123456789",
    },
  },
  platform: "Moodle 3.8.3",
};

const extensions = {
  "https://w3id.org/xapi/netc/extensions/school-center":
    "Center for Naval Aviation Technical Training (CNATT)",
  "https://w3id.org/xapi/navy/extensions/launch-location": "Ashore",
  "https://w3id.org/xapi/netc/extensions/user-agent": navigator.userAgent,
};

// Create ElearningXAPI object 
const xapi = new ELearningXAPI(config);

const { COURSE, LESSON } = xapi.ACTIVITY;
const { INTERACTION } = xapi;

xapi.addExtensions(extensions);

// to be used with interaction responses
interactionParent = xapi.createParent(
  "https://navy.mil/netc/xapi/activities/assessments/9e7623ad-0256-451c-9974-4e05033c53c1",
  "Naval Aviation Quiz",
  "This is an example assessment on Naval aviation history."
);

let courseRegistration, lessonRegistration;

const greet = greetings => console.log(greetings)

const form = document.getElementById("form");
const myFiles = document.getElementById("myFile");

form.addEventListener("submit", async (e) => {
  e.preventDefault();
  const attachmentBtn = document.getElementById("attachment-button");
  $(attachmentBtn).prop("disabled", true);
  // add spinner to button
  var reader = new FileReader();
  reader.onload = async function () {
    var arrayBuffer = this.result;
    // console.log("attachment", arrayBuffer);
    // send file attachment to the lrs
    getResult("other-upload-attachment", { fileName: myFiles.files[0].name, data: arrayBuffer });
  };
  reader.readAsArrayBuffer(myFiles.files[0]);
});

const styleToString = (style) => {
  return Object.keys(style).reduce((acc, key) => (
    acc + key.split(/(?=[A-Z])/).join('-').toLowerCase() + ':' + style[key] + ';'
  ), '');
};

const log = (text, css) => console.log(`%c ${text}`, styleToString(css));

// responsePattern 
const choiceResponse = [
  { id: "answer1", description: "answer1 example" },
  { id: "answer2", description: "answer2 example" },
  { id: "answer3", description: "answer3 example" },
].map(item => xapi.createInteractionComponentList(item.id, item.description));

const likertResponse = [
  { id: "option1", description: "OK" },
  { id: "option2", description: "Pretty Cool" },
  { id: "option3", description: "Gonna Change the World" }
].map(item => xapi.createInteractionComponentList(item.id, item.description));

const matchingSourceResponse = [
  { id: "source1", description: "5 + 5 =" },
  { id: "source2", description: "10 + 10 =" },
].map(item => xapi.createInteractionComponentList(item.id, item.description));

const matchingTargetResponse = [
  { id: "target1", description: "10" },
  { id: "target2", description: "20" },
].map(item => xapi.createInteractionComponentList(item.id, item.description));

const performanceResponse = [
  { id: "step1", description: "up on controller" },
  { id: "step2", description: "down on controller" },
].map(item => xapi.createInteractionComponentList(item.id, item.description));

const sequencingResponse = [
  { id: "1", description: "item 1 id" },
  { id: "2", description: "item 2 id" },
  { id: "3", description: "item 3 id" },
  { id: "4", description: "item 4 id" },
  { id: "5", description: "item 5 id" },
  { id: "6", description: "item 6 id" },
].map(item => xapi.createInteractionComponentList(item.id, item.description));

// TODO: ADD AN ARRAY OF ACTIVITIES TO RESTORE ON RESUME
const activities = [
  {
    id: "https://navy.mil/netc/xapi/activities/lessons/9dd5f696-96dd-49fa-a450-bcc8e36ae57e",
    name: "Lesson 1 Attempts",
    description: "An interactive e-learning course on how a course implements xAPI Statements."
  },
  {
    id: "https://navy.mil/netc/xapi/activities/lessons/1677e692-5820-4423-b8fe-98da2ab78574",
    name: "Lesson 2 Attempts",
    description: "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
  }
]
xapi.setActivity(
  "https://navy.mil/netc/xapi/activities/lessons/9dd5f696-96dd-49fa-a450-bcc8e36ae57e",
  "Lesson 1 Attempts",
  "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
);
xapi.setGroup(
  "https://navy.mil/netc/xapi/activities/courses/d5c891ad-355e-4e44-a3c9-c69b632f59b2",
  "E-Learning Content Example",
  "An interactive e-learning course on how a course implements xAPI Statements."
);

async function startApplication() {
  await xapi.launch()
    .then(result => console.log('Launch successful', result))
    .catch(error => `An error has occurred when launching the ELearning Library ${error}`)
}

startApplication()

async function getResult(id, options) {

  let result = null;
  let response = null;
  let interaction = null;
  let error = null
  let registration
  switch (id) {
    case "initialize-course-attempt":
      await xapi.initialize(COURSE).then(result => log(`${result} initialized course!`, { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "initialize-lesson-1-attempt":
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/9dd5f696-96dd-49fa-a450-bcc8e36ae57e",
        "Lesson 1 Attempts",
        "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
      );
      await xapi.initialize(LESSON).then(result => log(`${result} initialized course!`, { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "initialize-lesson-2-attempt":
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/1677e692-5820-4423-b8fe-98da2ab78574",
        "Lesson 2 Attempts",
        "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
      );
      await xapi.initialize(LESSON).then(result => log(`${result} initialized course!`, { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "viewed":
      response = await xapi
        .viewed(
          "https://navy.mil/netc/xapi/activities/pages/a0db418e-961d-416d-9051-49ac3f812bc2",
          "Page 1",
          "Page 1 of the Advanced Airborne Sensor Introduction section."
        )
        .then(result => log('viewed page 1', { color: '#C7B6DC' }))
        .catch((error) => log(error, { color: 'red' }));
      break;
    case "section":
      response = await xapi
        .section(
          "https://navy.mil/netc/xapi/activities/sections/edcc28de-ac32-11ea-bb37-0242ac130002",
          "Advanced Airborne Sensor Introduction",
          "The introduction section for the Advanced Airborne Sensor lesson."
        )
        .then(result => log('section complete', { color: '#C7B6DC' }))
        .catch((error) => log(error, { color: 'red' }));
      break;
    case "exit-section":
      response = await xapi
      .exit(
        "https://navy.mil/netc/xapi/activities/sections/edcc28de-ac32-11ea-bb37-0242ac130002",
        "Advanced Airborne Sensor Introduction",
        "The introduction section for the Advanced Airborne Sensor lesson."
      )
      .then(result => log('section complete', { color: '#C7B6DC' }))
      .catch((error) => log(error, { color: 'red' }));
    break;
    case "course-registration-data":
      // response = xapi.getCourseRegistration();
      document.getElementById(id).innerHTML = xapi.getCourseRegistration();
      break;
    case "registration-data":
      // response = xapi.getLessonRegistration();
      document.getElementById(id).innerHTML = xapi.getLessonRegistration();
      break;
    case "get-bookmark-data":
      const bookmark = xapi.getBookmark();
      document.getElementById(id).innerHTML = bookmark ? bookmark : 'no bookmark found'
      break;
    case "get-activity-data":
      response = await xapi.getActivityState("https://navy.mil/netc/xapi/activities/courses/d5c891ad-355e-4e44-a3c9-c69b632f59b2")
      document.getElementById('set-activity-data').innerHTML = JSON.stringify(response, undefined, 2)
      break;
    case "set-bookmark-data":
      result = document.getElementById("bookmark-input").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      response = await xapi.setBookmark(result).then(result => log(`${result} bookmarked!`, { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }))
      break;
    case "suspend-attempt-1-data":
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/9dd5f696-96dd-49fa-a450-bcc8e36ae57e",
        "Lesson 1 Attempts",
        "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
      );
      response = await xapi.suspend()
        .then(result => log('lesson suspended!', { color: '#C7B6DC' }))
        .catch(error => log(error, { color: 'red' }));
      break;
    case "suspend-attempt-2-data":
      xapi.setActivity(
        "https://navy.mil/netc/xapi/activities/lessons/1677e692-5820-4423-b8fe-98da2ab78574",
        "Lesson 2 Attempts",
        "How an attempt on a lesson works and how data about the lesson is expressed in xAPI Statements."
      );
      response = await xapi.suspend("https://navy.mil/netc/xapi/activities/lessons/1677e692-5820-4423-b8fe-98da2ab78574")
        .then(result => log('lesson suspended!', { color: '#C7B6DC' }))
        .catch(error => log(error, { color: 'red' }));
      break;
    case "resume-attempt-data":
      // var inputValue = document.getElementById("resume-input").value;
      const activityId = document.getElementById("resume-input").value
      console.log(activityId)
      if (activityId != '') {
        const found = activities.find(activity => activity.id === activityId)
        if (!found) activityId = null;

        xapi.setActivity(found.id, found.name, found.description);
      }
      response = await xapi.resume(activityId).then(result => log('lesson resumed!', { color: '#C7B6DC' }))
        .catch(err => {
          error = err
          console.log('resume', err);
        })
      break;
    case "terminate-lesson-attempt-data":
      response = await xapi.terminate(LESSON).then(result => log('course terminated!', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "terminate-course-attempt-data":
      response = await xapi.terminate(COURSE).then(result => log('lesson terminated!', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "completed-data":
      response = await xapi.setComplete(
        document.getElementById("completionList").value === "true" ? true : false
      ).then(result => log('completion set', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "success-data":
      response = await xapi.setSuccess(
        document.getElementById("successList").value === "true" ? true : false
      ).then(result => log('success set', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "scaled-score-data":
      result = document.getElementById("scaledScoreOnly").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      response = await xapi.setScore(xapi.SCORE.SCALED, Number(result)).then(result => log('scaled score set', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "raw-score-data":
      result = document.getElementById("rawScoreOnly").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      response = await xapi.setScore(xapi.SCORE.RAW, Number(result)).then(result => log('raw score set', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "min-score-data":
      result = document.getElementById("minScoreOnly").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      response = await xapi.setScore(xapi.SCORE.MIN, Number(result));
      console.log("min-score-data", response)
      break;
    case "max-score-data":
      result = document.getElementById("maxScoreOnly").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      response = await xapi.setScore(xapi.SCORE.MAX, Number(result));
      break;
    case "true-false-data":
      interaction = xapi.createInteraction(
        xapi.INTERACTION.TRUE_FALSE,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q1",
        "True False Question",
        "True False Question Description",
        ["true"]
      );

      response = await xapi.sendInteraction(
        [document.getElementById("tf_true").checked.toString()],
        interaction,
        interactionParent
      ).then(result => log(`true-false interaction statement sent, ${result}`, { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "choice-data":
      interaction = xapi.createInteraction(
        xapi.INTERACTION.CHOICE,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q2",
        "Multiple Choice Question",
        "Multiple Choice Question Description",
        xapi.createResponsePattern(INTERACTION.CHOICE, ["answer1"]),
        choiceResponse
      );
      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.CHOICE, getAllChecked(getAllTagTypes("multiple-choice", "input"))),
        interaction,
        interactionParent
      ).then(result => log('choice interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "fill-in-data":
      result = document.getElementById("fi_text").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      interaction = xapi.createInteraction(
        INTERACTION.FILL_IN,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q3",
        "Fill-In Question",
        "Fill-In Question Description",
        ["Bob is your uncle"]
      );
      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.FILL_IN, [result]),
        interaction,
        interactionParent
      ).then(result => log('fill-in interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "long-fill-in-data":
      result = document.getElementById("lfi_text").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      interaction = xapi.createInteraction(
        INTERACTION.LONG_FILL_IN,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q4",
        "Long Fill-In Question",
        "Long Fill-In Question Description",
        [
          "{case_matters=false}{lang=en}To store and provide access to learning experiences.",
        ]
      );
      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.LONG_FILL_IN, [result]),
        interaction,
        interactionParent
      ).then(result => log('long-fill-in interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "likert-data": log(getLikertInteractionResponse(), { color: '#C7B6DC' })
      interaction = xapi.createInteraction(
        INTERACTION.LIKERT,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q5",
        "Likert Question",
        "Likert Question Description",
        ["option3"],
        likertResponse
      );
      response = await xapi.sendInteraction(
        [getLikertInteractionResponse()],
        interaction,
        interactionParent
      ).then(result => log('likert interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "matching-data":
      interaction = xapi.createInteraction(
        INTERACTION.MATCHING,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q6",
        "Matching Question",
        "Matching Question Description",
        xapi.createResponsePattern(INTERACTION.MATCHING, ["target1:source2", "target2:source1"]),
        { source: matchingSourceResponse, target: matchingTargetResponse }
      );
      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.MATCHING, getMatchingState().result),
        interaction,
        interactionParent
      ).then(result => log('matching interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "performance-data":
      result = document.getElementById("performance-input").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }

      interaction = xapi.createInteraction(
        INTERACTION.PERFORMANCE,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q7",
        "Performance Question",
        "Performance Question Description",
        xapi.createResponsePattern(INTERACTION.PERFORMANCE, ["step1:up", "step2:down"]),
        performanceResponse
      );
      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.PERFORMANCE, result.replaceAll('"', '').split(",")),
        interaction,
        interactionParent
      ).then(result => log('performance interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "sequencing-data":
      result = document.getElementById("sequencing-input").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      interaction = xapi.createInteraction(
        INTERACTION.SEQUENCING,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q8",
        "Sequencing Question",
        "Sequencing Question Description",
        xapi.createResponsePattern(INTERACTION.SEQUENCING, ['1', '2', '3', '4', '5', '6']),
        sequencingResponse
      );

      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.SEQUENCING, result.split(",")),
        interaction,
        interactionParent
      ).then(result => log('sequencing interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "numeric-data":
      result = document.getElementById("numeric-input").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      interaction = xapi.createInteraction(
        INTERACTION.NUMERIC,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q9",
        "Numeric Question",
        "Numeric Question Description",
        [':4']
      );

      response = await xapi.sendInteraction(
        xapi.createResponsePattern(INTERACTION.NUMERIC, [result]),
        interaction,
        interactionParent
      ).then(result => log('numeric interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case "other-data":
      result = document.getElementById("other-input").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      interaction = xapi.createInteraction(
        INTERACTION.OTHER,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q10",
        "Other Question",
        "Other Question Description"
      );
      response = await xapi.sendInteraction(
        [result],
        interaction,
        interactionParent
      ).then(result => log('other interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case 'other-upload':
      result = document.getElementById("other-upload").value;
      if (result === "") {
        alert("Really? Enter a value.");
        return;
      }
      interaction = xapi.createInteraction(
        INTERACTION.OTHER_UPLOAD,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q11",
        "Other Question Upload",
        "Other Question Upload Description"
      );
      response = await xapi.sendInteraction(
        [result],
        interaction,
        interactionParent,
      ).then(result => log('other-upload interaction statement sent', { color: '#C7B6DC' })).catch(error => log(error, { color: 'red' }));
      break;
    case 'other-upload-attachment':
      const { fileName, data } = options;
      const attachment = xapi.attachment(
        data,
        "image",
        "image/jpeg",
        "Image Attachment.",
        "Uploading An Image Attachment."
      );
      interaction = xapi.createInteraction(
        INTERACTION.OTHER_UPLOAD_ATTACHMENT,
        "https://navy.mil/netc/courses/xapi101/overview/assessment/q12",
        "Other Question Upload Attachment",
        "Other Question Upload Attachment Description"
      );
      response = await xapi.sendInteraction(
        [fileName],
        interaction,
        interactionParent,
        null,
        [attachment]
      ).then(result => console.log(result)).catch(error => log(error, { color: 'red' }));
      break;
    default:
      break;
  }
  if (error === null) console.log(`%c ${xapi.statement}`, 'color: #B6DCC7');

  await xapi.getActivityState("https://navy.mil/netc/xapi/activities/courses/d5c891ad-355e-4e44-a3c9-c69b632f59b2")
    .then(result => log(JSON.stringify(result, undefined, 2), { color: 'yellow' }))
    .catch(error => log(error, { color: 'red' }))
}
async function resumeAttempt() {
  const result = await xapi.resume();
  const attemptDiv = document.getElementById("attempt-data");
  attemptDiv.innerHTML = result;
}

function getAllTagTypes(id, tag) {
  // grab all input elements from parent id div
  const inputs = document.getElementById(id);
  const tags = inputs.getElementsByTagName(tag);

  return tags;
}
function getAllChecked(inputs) {
  //spread htmlcollection into an array then filter & return checked input values
  return [...inputs]
    .filter((input) => input.checked)
    .filter((checked) => checked.value)
    .map((selected) => selected.value);
}
// gets the response from the likert question
function getLikertInteractionResponse() {
  // grab all input elements from the likert-inputs div
  const inputs = document.getElementById("likert-inputs");
  const tags = inputs.getElementsByTagName("input");

  //spread htmlcollection into an array then filter & return checked input value
  return [...tags].filter((input) => input.checked)[0].value;
}
