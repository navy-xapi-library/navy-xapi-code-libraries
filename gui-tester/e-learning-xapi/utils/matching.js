// grab drag and droppable containers
let one = document.querySelector('#container1')
let two = document.querySelector('#container2')
let three = document.querySelector('#container3')

const state = {
    result: []
}

const setState = (value) => {
    state.result.push(value)
}
/*
    A list of matching pairs, where each pair consists of a source item id followed by a target item id. 
    Items can appear in multiple (or zero) pairs. Items within a pair are delimited by [.]. Pairs are delimited by [,].
    "correctResponsesPattern": [
        "ben[.]3[,]chris[.]2[,]troy[.]4[,]freddie[.]1"
    ],
*/
// const getMatchingState = () => state.result.reduce( (previousValue, currentValue, idx, src) => {
//     previousValue += (idx === src.length-1) ? `${currentValue.target}[.]${currentValue.source}` :`${currentValue.target}[.]${currentValue.source}[,]`;
//     return previousValue
// },"");

const getMatchingState = () => {
    // console.log("getMatchingState", state)
    return state
};

// instantiate dragula! MUHAHAHA!
var drake = dragula([one, two, three], {
    revertOnSpill: true,
    copy: false,
});

// 'on' drag and drop event handlers
drake.on('drag', function (el, source) {
    // document.getElementsByTagName('body')[0].style.backgroundColor = '#28a0ef';
})
drake.on('drop', function (el, target) {
    el.style.backgroundColor = "#0062cc";
    el.style.color = "white";
    el.style.border = "none"
    el.style.pointerEvents = "none"
    target.style.backgroundColor = "#0062cc";

    setState( `${target.getAttribute('data-id')}:${el.getAttribute('data-id')}`)
    //   console.log(state.result)
});