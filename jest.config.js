module.exports = {
    // transform: {
    //     "^.+\\.[t|j]sx?$": "babel-jest"
    // },
    transformIgnorePatterns: [
        '[/\\\\]node_modules[/\\\\](?!aggregate-error|clean-stack|escape-string-regexp|indent-string|p-map).+\\.(js|jsx)$',
      ],
      globals: {
        'ts-jest': {
          babelConfig: true
        }
      }
  };