# **Navy xAPI Code Libraries** 
A set of Javascript Libraries to help simplify communication to an LRS and to help track interactions and learning activities at a granular level as well as provide the tools to help aid in one's Performance.

# **Dependencies**
In order to start using the library install git. Git is a version control system used to track changes in source code during development.

- Download and install Git here https://git-scm.com/   

Use git to clone the veracity-xapi-libraries. Open up an terminal window and run the below command.

```
git clone https://bitbucket.org/netc-lms-lrs/navy-xapi-code-libraries.git
```

# **Developers**
For developers who are interested in updating the library a few dependencies are needed to run and perform a rebuild of the library.

- Download and install Node.js here https://nodejs.org/
  
When installing Node.js, Node's Node package manager (npm) is automatically installed and will be used to install some dependencies in the next section.

### Installing
After cloning the repository, open the repository location and run the following command. 
```
npm install 

// mac user may need to use sudo
sudo npm install

// if you prefer and have "yarn" installed then run the below command to install the dependencies
yarn 

// mac user may need to use sudo
sudo yarn
```
Running "npm install or yarn" will install all of the dependencies needed to update the library. 

### Updating
For developers wanting to update the library the files to update can be found in the src folder in the root directory. The library was built using typescript, so knowledge of typescript helps with developing new library features. You can find more information on typescript here https://www.typescriptlang.org/. Typescript is automatically install when running the command from the "Installing" section. 

**Note:** The xapiWrapper directory within the src directory is a 3rd party library the E-LearningXAPI and PeformanceSupportXAPI libraries use and should **not be updated**. 

What will you find in the src folder:

| Directory              | Description                                                            |
|:---------------------- | :--------------------------------------------------------------------  |
| e-learning-xapi        | Directory that holds the source code for the ELearningXAPI library.    |
| performance-xapi       | Directory that holds the source code for the PerformanceXAPI library.  |
| xapiWrapper            | Directory that holds the 3rd party xapiWrapper files.                  | 
| index.ts               | File that exports both ELearningXAPI and PerformanceXAPI libraries.    | 

Both the ELearningXAPI and PerformanceXAPI libraries contain the following directories.

| Directory              | Description                                                            |
|:---------------------- | :--------------------------------------------------------------------  |
| app                    | Directory that holds the main application file.                        |
| constants              | Directory that holds all constants used within the library.            |
| statements             | Directory that holds the statement builder file.                       |
| utils                  | Directory that holds the utility file.                                 | 

### Building the project
Once all of the dependencies have been installed running the build script is fairly easy.
Run the below command to build out the library.

```
  npm run build 
  
  or

  yarn build
```
Running the build command will rebuild and minified the library. The location of the new build is inside of the dist directory in the root directory. There you will find the veracity.umd.production.min.js file along with some other helpful files. 

What will you find in the dist folder:

| File                                  | Description                                                              |
|:------------------------------------- | :----------------------------------------------------------------------  |
| e-learning-xapi                       | Directory that holds the declaration files for the e-learning-xapi lib.  |
| performance-xapi                      | Directory that holds the declaration files for the performance-xapi lib. |
| veracity.umd.production.min.js        | The minimized version of the library.                                    |
| veracity.umd.production.min.js.map    | The sourcemap file use to debugging a production build of the library.   |
| index.d.ts                            | Library declaration file.                                                |
| package.json                          | The file that holds various metadata relevant to the library             |

***

# **Documentation**
Use the following links to learn about how to use the E-LearningXAPI and PeformanceSupportXAPI libraries.

[E-LearningXAPI Library https://bitbucket.org/netc-lms-lrs/e-learning-xapi-code-library-documentation ]

[PerformanceXAPI Library https://bitbucket.org/netc-lms-lrs/performance-support-xapi-code-library-documentation ]


